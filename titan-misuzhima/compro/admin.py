# from django.contrib import admin
# from .models import HeroImage, DeskripsiSingkat, DeskripsiLengkap, NilaiPerusahaan, Testimoni, FotoGaleri, VisiMisi, \
#     ProfilPetinggi, Alamat, Kontak, SosialMedia, KebijakanMutu, StrukturOrganisasi, InfoKarir


# class SingletonModelAdmin(object):
#     def has_add_permission(self, request):
#         num_objects = self.model.objects.count()
#         if num_objects >= 1:
#             return False
#         return super(SingletonModelAdmin, self).has_add_permission(request)


# # Register your models here.
# @admin.register(HeroImage)
# class HeroImageAdmin(admin.ModelAdmin):
#     list_display = ('id', 'gambar')
#     list_display_links = ('id', 'gambar')
#     list_per_page = 25


# @admin.register(DeskripsiSingkat)
# class DeskripsiSingkatAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_display = ('id', 'deskripsi')
#     list_display_links = ('id', 'deskripsi')
#     list_per_page = 25


# @admin.register(NilaiPerusahaan)
# class NilaiPerusahaanAdmin(admin.ModelAdmin):
#     list_diplay = ('gambar', 'judul', 'deskripsi')
#     list_diplay_links = ('gambar',)
#     search_fields = ('judul',)
#     list_per_page = 25


# @admin.register(Testimoni)
# class TestimoniAdmin(admin.ModelAdmin):
#     list_diplay = ('foto', 'nama', 'jabatan', 'deskripsi')
#     list_diplay_links = ('foto',)
#     search_fields = ('nama', 'jabatan')
#     list_per_page = 25


# @admin.register(FotoGaleri)
# class FotoGaleriAdmin(admin.ModelAdmin):
#     list_diplay = ('foto', 'deskripsi')
#     list_diplay_links = ('foto',)
#     search_fields = ('deskripsi',)
#     list_per_page = 25


# @admin.register(DeskripsiLengkap)
# class DeskripsiLengkapAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_display = ('id', 'deskripsi')
#     list_display_links = ('id', 'deskripsi')
#     list_per_page = 25


# @admin.register(VisiMisi)
# class VisiMisiAdmin(admin.ModelAdmin):
#     list_display = ('jenis', 'deskripsi')
#     search_fields = ('jenis', 'deskripsi')
#     list_per_page = 25


# @admin.register(StrukturOrganisasi)
# class StrukturOrganisasiAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_display = ('id', 'gambar')
#     search_fields = ('id', 'gambar')
#     list_per_page = 25


# @admin.register(ProfilPetinggi)
# class ProfilPetinggiAdmin(admin.ModelAdmin):
#     list_diplay = ('foto', 'nama', 'jabatan', 'deskripsi')
#     list_diplay_links = ('foto',)
#     search_fields = ('nama', 'jabatan')
#     list_per_page = 25


# @admin.register(Alamat)
# class AlamatAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_diplay = ('id', 'deskripsi')
#     search_fields = ('id', 'deskripsi')
#     list_per_page = 25


# @admin.register(Kontak)
# class KontakAdmin(admin.ModelAdmin):
#     list_display = ('jenis', 'nomor')
#     search_fields = ('jenis', 'nomor')
#     list_per_page = 25


# @admin.register(SosialMedia)
# class SosialMediaAdmin(admin.ModelAdmin):
#     list_display = ('jenis', 'url')
#     search_fields = ('jenis', 'url')
#     list_per_page = 25


# @admin.register(KebijakanMutu)
# class KebijakanMutuAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_display = ('deskripsi',)
#     list_display_links = ('deskripsi',)
#     list_per_page = 25


# @admin.register(InfoKarir)
# class InfoKarirAdmin(SingletonModelAdmin, admin.ModelAdmin):
#     list_display = ('deskripsi',)
#     list_display_links = ('deskripsi',)
#     list_per_page = 25
