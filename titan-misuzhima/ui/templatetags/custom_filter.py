from django import template
from django.contrib.humanize.templatetags.humanize import intcomma

import locale

register = template.Library()


@register.filter(name='rupiah')
def rupiah_format(rupiahs):
    try:
        rupiahs = round(float(rupiahs), 2)
        temp = "Rp %s%s" % (intcomma(int(rupiahs)), ("%0.2f" % rupiahs)[-3:])
        temp = temp.replace('.', 'x')
        temp = temp.replace(',', '.')
        temp = temp.replace('x', ',')
    except:
        temp = "Rp 0,00"
    return temp


@register.filter(name='round_rupiah')
def round_rupiah_format(rupiahs):
    try:
        rupiahs = round(float(rupiahs), 2)
        temp = "%s" % (intcomma(int(rupiahs)))
        temp = temp.replace('.', 'x')
        temp = temp.replace(',', '.')
        temp = temp.replace('x', ',')
    except:
        temp = "0"

    return temp if temp != '0' else '-'


@register.filter(name='truncate')
def truncate_name(name):
    try:
        display = ''
        namelist = name.split()
        if len(namelist) > 1:
            for i in range(len(namelist)):
                if (i == 1 and len(namelist[i]) > 7) or i > 1:
                    namelist[i] = f'{namelist[i][0]}.'
        display = ' '.join(namelist)
    except:
        pass
    return display


@register.filter(name='currency')
def rupiah_format(angka):
    try:
        angka = int(float(angka))
        locale.setlocale(locale.LC_NUMERIC, 'id_ID.utf8')
        rupiah = locale.format("%d", (angka), True)
    except:
        rupiah = 0
    return "{}".format(rupiah)


@register.filter
def jumlah(value, arg):
    try:
        value = int(float(value))
    except:
        value = 0

    try:
        arg = int(float(arg))
    except:
        arg = 0

    return value + arg
