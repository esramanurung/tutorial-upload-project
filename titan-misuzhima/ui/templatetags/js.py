from django.utils.safestring import mark_safe
from django.template import Library

import json
import datetime

register = Library()


@register.filter(is_safe=True)
def js(obj):
        if (isinstance(obj, datetime.datetime)):
                obj = datetime.datetime.strftime('%d-&b-%Y')
        return mark_safe(json.dumps(obj, default=str))
