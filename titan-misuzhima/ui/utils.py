from openpyxl import load_workbook

from ui.models import *
from ui.constant import *
from ui.constant_2 import *
from ui.bindings import *
from ui.bindings_2 import *

from dateutil.relativedelta import *
from django.db.models import Sum
from django.db.models.functions import Cast
from django.db.models import FloatField

from django.contrib.humanize.templatetags.humanize import intcomma

import datetime


def import_user(file):
    book = load_workbook(file, data_only=True)
    sheet = book.active
    max_row = sheet.max_row
    label_range = sheet['B4:BG4']
    label = [x.value for x in label_range[0]]
    arr_csv = []
    arr_db = []
    ranges = 'A6:BG%s' % (str(max_row))
    for datas in sheet[ranges]:
        _nopeg = datas[nopeg].value
        exist = check_obj_staff(_nopeg)
        if exist:
            arr_csv.append([d.value for d in datas[nopeg:]])
            arr_db.append(get_obj_staff(_nopeg))
        else:
            save_obj_staff(datas)

    return {'label': label, 'staff_db': arr_db, 'staff_csv': arr_csv}


def import_monthly_payroll(file, month, tipe):
    book = load_workbook(file, data_only=True)
    sheet = book.active
    max_row = sheet.max_row
    label_range = sheet['B4:Y4']
    label = [x.value for x in label_range[0]]
    arr_csv = []
    arr_db = []
    ranges = 'A6:BF%s' % (str(max_row))
    for datas in sheet[ranges]:
        _nopeg = datas[nopeg].value
        staff_exist = check_obj_staff(_nopeg)
        payroll_exist = check_obj_payroll(_nopeg, month, tipe)
        if staff_exist and not payroll_exist:
            save_obj_payroll(datas, month, tipe)
        elif staff_exist and payroll_exist:
            if check_payroll_published(month, tipe):
                return {'error': 'Payroll already published'}
            update_obj_payroll(datas, month, tipe, penghitung='dipilih')
        else:
            pass

    return {'label': label, 'error': ''}


def rupiah_format(rupiahs):
    try:
        rupiahs = round(float(rupiahs), 2)
        temp = "%s" % (intcomma(int(rupiahs)))
        temp = temp.replace('.', 'x')
        temp = temp.replace(',', '.')
        temp = temp.replace('x', ',')
    except:
        temp = "0"

    return temp

# CHECK POINT DEBUG

    # print(datas[0].value)
    # # try:
    # staff_is_exist = check(datas[0].value)
    # if not staff_is_exist:
    #     save(datas)
    # else:
    #     staff_db = get_staff(datas[0].value)
    #     staff_csv = [x.value for x in datas]
    #     arr_csv.append(staff_csv)
    #     arr_db.append(staff_db)

    # update(datas)
    # except Error:
    #     print(Error)
    #     print(max_row)
    # ranges = 'B6:AS%s' % (str(max_row))
    # for datas in sheet[ranges]:
    #     try:
    #         staff_is_exist = check(datas[0].value)
    #         if not staff_is_exist:
    #             save)


# def retrieve_payroll(files, month):
#     book = load_workbook(files, data_only=True)
#     sheet = book.active
#     max_row = sheet.max_row
#     ranges = 'B6:AA%s' % (str(max_row))
#     for datas in sheet[ranges]:
#         staff_is_exist = check(datas[0].value)
#         if staff_is_exist:
#             save_payroll(datas, month)


# def create(datas):
#     staff = Staff(
#         username=datas[0],
#         password=Staff.objects.make_random_password(8),
#         nopeg=datas[0],
#         nama_pegawai=datas[1],
#         npwp=datas[2],
#         jabatan=datas[3],
#         no_kontrak=datas[4],
#         tanggal_mulai=datetime.datetime.strptime(
#             datas[5], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         masa_kontrak=datetime.datetime.strptime(
#             datas[6], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         perusahaan=datas[7],
#         departemen=datas[8],
#         departemen_sekarang=datas[9],
#         supervisi=datas[10],
#         lokasi=datas[11],
#         no_id_card=datas[12],
#         masa_berlaku=datetime.datetime.strptime(
#             datas[13], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         tempat_lahir=datas[14],
#         tanggal_lahir=datetime.datetime.strptime(
#             datas[15], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         alamat_sekarang=datas[16],
#         no_ktp=datas[17],
#         email_bas=datas[18],
#         email_pribadi=datas[19],
#         # kondisi="tidak terpakai, diganti kondisi aktif/non-aktif", # tidak dipakai
#         status=datas[21],
#         no_telpon=datas[22],
#         nik=datas[23],
#         nama_istri=datas[24],
#         nama_anak1=datas[25],
#         nama_anak2=datas[26],
#         nama_anak3=datas[27],
#         bpjs_kesehatan=datas[28],
#         masa_bpjs_kesehatan=datas[29],
#         bpjs_tenaga_kerja=datas[30],
#         masa_bpjs_tenaga_kerja=datas[31],
#         no_polis=datas[32],
#         pendidikan_terakhir=datas[33],
#         sertifikat1=datas[34],
#         sertifikat2=datas[35],
#         sertifikat3=datas[36],
#         sertifikat4=datas[37],
#         training_diikuti=datas[38],
#         training1=datetime.datetime.strptime(
#             datas[39], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         training2=datetime.datetime.strptime(
#             datas[40], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         training3=datetime.datetime.strptime(
#             datas[41], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         training4=datetime.datetime.strptime(
#             datas[42], '%d-%b-%Y').strftime('%Y-%m-%d'),
#         kelamin=datas[43],
#     )

#     staff.save()

#     staff_foreignkey = Staff.objects.get(nopeg=datas[0])

#     if datas[44] == 'aktif':
#         status_staff = StatusStaff(
#             staff=staff_foreignkey,
#             status_pegawai=datas[44],
#             catatan_aktif=datas[45],
#             tanggal_mulai=datas[46],
#             tanggal_berakhir=datas[47],
#             keterangan_aktif=datas[48],
#             catatan_nonaktif=None,
#             tanggal_resign=None,
#             pengganti=None,
#             keterangan_nonaktif=None,
#         )
#     else:
#         status_staff = StatusStaff(
#             staff=staff_foreignkey,
#             status_pegawai=datas[44],
#             catatan_aktif=None,
#             tanggal_mulai=None,
#             tanggal_berakhir=None,
#             keterangan_aktif=None,
#             catatan_nonaktif=datas[45],
#             tanggal_resign=datas[46],
#             pengganti=datas[47],
#             keterangan_nonaktif=datas[48],
#         )

#     status_staff.save()

#     history = History(
#         status="[ADD STAFF] %s -  %s" % (staff.nopeg, staff.nama_pegawai)
#     )

#     history.save()


# def create_payroll(datas):
#     staff = Staff.objects.get(nopeg=datas[0])
#     payroll = Payroll(
#         staff=staff,
#         gaji_pokok=datas[1],
#         tunjangan_jabatan=datas[2],
#         tunjangan_kehadiran_osa=datas[3],
#         tunjangan_lisensi=datas[4],
#         tunjangan_lainnya=datas[5],
#         lembur=datas[6],
#         tunjangan_pph_21=datas[7],
#         total_penghasilan=datas[8],
#         bpjs_kesehatan=datas[9],
#         bpjs_ketenagakerjaan=datas[10],
#         jaminan_pensiun=datas[11],
#         pot_pinjaman_1=datas[12],
#         pot_pinjaman_2=datas[13],
#         pot_pinjaman_3=datas[14],
#         simpanan_pokok_koperasi=datas[15],
#         simpanan_wajib_koperasi=datas[16],
#         pot_lainnya=datas[17],
#         pph_21=datas[18],
#         total_potongan=datas[19],
#         gaji_bersih=datas[20],
#         akumulasi_simpanan_pokok=datas[21],
#         akumulasi_simpanan_wajib=datas[22],
#         sisa_hutang_pinjaman=datas[23],
#         nomor_rekening=datas[24],
#         bulan=datas[25],
#         status="Not Published",
#     )

#     try:
#         payroll.save()

#         history = History(
#             status="[ADD PAYROLL] %s - %s" % (
#                 payroll.staff.nama_pegawai, payroll.bulan)
#         )

#         history.save()

#     except:
#         pass


# def update(datas):
#     staff = Staff.objects.get(nopeg=datas[0])
#     staff.nama_pegawai = datas[1]
#     staff.npwp = datas[2]
#     staff.jabatan = datas[3]
#     staff.no_kontrak = datas[4]
#     staff.tanggal_mulai = datas[5]
#     staff.masa_kontrak = datas[6]
#     staff.perusahaan = datas[7]
#     staff.departemen = datas[8]
#     staff.departemen_sekarang = datas[9]
#     staff.supervisi = datas[10]
#     staff.lokasi = datas[11]
#     staff.no_id_card = datas[12]
#     staff.masa_berlaku = datas[13]
#     staff.tempat_lahir = datas[14]
#     staff.tanggal_lahir = datas[15]
#     staff.alamat_sekarang = datas[16]
#     staff.no_ktp = datas[17]
#     staff.email_bas = datas[18]
#     staff.email_pribadi = datas[19]
#     staff.no_telpon = datas[20]
#     staff.nik = datas[21]
#     staff.nama_istri = datas[22]
#     staff.nama_anak1 = datas[23]
#     staff.nama_anak2 = datas[24]
#     staff.nama_anak3 = datas[25]
#     staff.bpjs_kesehatan = datas[26]
#     staff.masa_bpjs_kesehatan = datas[27]
#     staff.bpjs_tenaga_kerja = datas[28]
#     staff.masa_bpjs_tenaga_kerja = datas[29]
#     staff.no_polis = datas[30]
#     staff.pendidikan_terakhir = datas[31]
#     staff.sertifikat1 = datas[32]
#     staff.sertifikat2 = datas[33]
#     staff.sertifikat3 = datas[34]
#     staff.sertifikat4 = datas[35]
#     staff.training_diikuti = datas[36]
#     staff.training1 = datas[37]
#     staff.training2 = datas[38]
#     staff.training3 = datas[39]
#     staff.training4 = datas[40]
#     staff.referensi = datas[41]
#     staff.status = datas[42]
#     staff.kelamin = datas[43]

#     staff.save()

#     staff_foreignkey = StatusStaff.objects.get(staff__nopeg=datas[0])
#     staff_foreignkey.status_pegawai = datas[44]
#     staff_foreignkey.catatan_aktif = datas[45]
#     staff_foreignkey.tanggal_mulai = datas[46]
#     staff_foreignkey.tanggal_berakhir = datas[47]
#     staff_foreignkey.keterangan_aktif = datas[48]
#     staff_foreignkey.catatan_nonaktif = datas[49]
#     staff_foreignkey.tanggal_resign = datas[50]
#     staff_foreignkey.pengganti = datas[51]
#     staff_foreignkey.keterangan_nonaktif = datas[52]

#     staff_foreignkey.save()


# def save_obj_staff(data):
#     try:
#         staff = Staff(
#             username=data[nopeg].value,
#             password=Staff.objects.make_random_password(8),
#             nopeg=data[nopeg].value,
#             nama_pegawai=data[nama_pegawai].value,
#             npwp=data[npwp].value,
#             nik=data[nik].value,
#             basis_gaji=convert_to_int(data[basis_gaji].value),
#             jenis_kelamin=data[jenis_kelamin].value,
#             status_pajak=data[status_pajak].value,
#             status_kepegawaian=data[status_kepegawaian].value,
#             dept_code=data[dept_code].value,
#             gl_account=data[gl_account].value,
#             group_expenses=data[group_expenses].value,
#             dept=data[dept].value,
#             golongan=data[golongan].value,
#             jabatan=data[jabatan].value,
#             tempat_lahir=data[tempat_lahir].value,
#             tanggal_lahir=convert_to_date(data[tanggal_lahir].value),
#             tanggal_mulai_kerja=convert_to_date(
#                 data[tanggal_mulai_kerja].value),
#             nomor_kontrak=data[nomor_kontrak].value,
#             masa_kontrak=data[masa_kontrak].value,
#             ditempatkan_di_perusahaan=data[ditempatkan_di_perusahaan].value,
#             under_supervisi=data[under_supervisi].value,
#             lokasi=data[lokasi].value,
#             nomor_id_card=data[nomor_id_card].value,
#             masa_berlaku_id_card=convert_to_date(
#                 data[masa_berlaku_id_card].value),
#             alamat_domisili_sekarang=data[alamat_domisili_sekarang].value,
#             alamat_email_perusahaan=data[alamat_email_perusahaan].value,
#             alamat_email_pribadi=data[alamat_email_pribadi].value,
#             nomor_telpon=data[nomor_telpon].value,
#             nomor_kartu_keluarga=data[nomor_kartu_keluarga].value,
#             nama_suami_istri=data[nama_suami_istri].value,
#             nama_anak_satu=data[nama_anak_satu].value,
#             nama_anak_dua=data[nama_anak_dua].value,
#             nama_anak_tiga=data[nama_anak_tiga].value,
#             nomor_bpjs_kesehatan=data[nomor_bpjs_kesehatan].value,
#             masa_berlaku_bpjs_kesehatan=convert_to_date(
#                 data[masa_berlaku_bpjs_kesehatan].value),
#             nomor_bpjs_tenagakerja=data[nomor_bpjs_tenagakerja].value,
#             masa_berlaku_bpjs_tenagakerja=convert_to_date(
#                 data[masa_berlaku_bpjs_tenagakerja].value),
#             premi_asuransi=data[premi_asuransi].value,
#             pendidikan_terakhir=data[pendidikan_terakhir].value,
#             sertifikat_satu=data[sertifikat_satu].value,
#             sertifikat_dua=data[sertifikat_dua].value,
#             sertifikat_tiga=data[sertifikat_tiga].value,
#             sertifikat_empat=data[sertifikat_empat].value,
#             training_diikuti=data[training_diikuti].value,
#             training_satu=data[training_satu].value,
#             training_dua=data[training_dua].value,
#             training_tiga=data[training_tiga].value,
#             training_empat=data[training_empat].value,
#             referensi=data[referensi].value,
#         )

#         staff.save()

#     except:
#         print('error save staff')

#     try:
#         staff_foreignkey = Staff.objects.get(nopeg=data[nopeg].value)

#         status_staff = StatusStaff(
#             staff=staff_foreignkey,
#             status_aktif=data[status_aktif].value,
#             status_aktif_berlaku=data[status_aktif_berlaku].value,
#             status_aktif_berakhir=data[status_aktif_berakhir].value,
#             keterangan_aktif=data[keterangan_aktif].value,
#             status_tidak_aktif=data[status_tidak_aktif].value,
#             finish_contract=data[finish_contract].value,
#             pengganti=data[pengganti].value,
#             keterangan_nonaktif=data[keterangan_nonaktif].value,
#         )

#         status_staff.save()

#     except:
#         print('error save status staff')


# def save_payroll(datas, month):
#     staff = Staff.objects.get(nopeg=datas[0].value)
#     payroll = Payroll(
#         staff=staff,
#         gaji_pokok=datas[2].value,
#         tunjangan_jabatan=datas[3].value,
#         tunjangan_kehadiran_osa=datas[4].value,
#         tunjangan_lisensi=datas[5].value,
#         tunjangan_lainnya=datas[6].value,
#         lembur=datas[7].value,
#         tunjangan_pph_21=datas[8].value,
#         total_penghasilan=datas[9].value,
#         bpjs_kesehatan=datas[10].value,
#         bpjs_ketenagakerjaan=datas[11].value,
#         jaminan_pensiun=datas[12].value,
#         pot_pinjaman_1=datas[13].value,
#         pot_pinjaman_2=datas[14].value,
#         pot_pinjaman_3=datas[15].value,
#         simpanan_pokok_koperasi=datas[16].value,
#         simpanan_wajib_koperasi=datas[17].value,
#         pot_lainnya=datas[18].value,
#         pph_21=datas[19].value,
#         total_potongan=datas[20].value,
#         gaji_bersih=datas[21].value,
#         akumulasi_simpanan_pokok=datas[22].value,
#         akumulasi_simpanan_wajib=datas[23].value,
#         sisa_hutang_pinjaman=datas[24].value,
#         nomor_rekening=datas[25].value,
#         bulan=month,
#         status="Not Published",
#     )

#     try:
#         payroll.save()

#         history = History(
#             status="[ADD PAYROLL] %s - %s" % (
#                 payroll.staff.nama_pegawai, payroll.bulan)
#         )

#         history.save()

#     except:
#         pass


# def edit(datas):

#     staff = Staff.objects.get(nopeg=datas[0])
#     staff.nama_pegawai = datas[1]
#     staff.npwp = datas[2]
#     staff.jabatan = datas[3]
#     staff.no_kontrak = datas[4]
#     staff.tanggal_mulai = datetime.datetime.strptime(
#         datas[5], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[5] not in ['', None] else None
#     staff.masa_kontrak = datetime.datetime.strptime(
#         datas[6], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[6] not in ['', None] else None
#     staff.perusahaan = datas[7]
#     staff.departemen = datas[8]
#     staff.departemen_sekarang = datas[9]
#     staff.supervisi = datas[10]
#     staff.lokasi = datas[11]
#     staff.no_id_card = datas[12]
#     staff.masa_berlaku = datetime.datetime.strptime(
#         datas[13], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[13] not in ['', None] else None
#     staff.tempat_lahir = datas[14]
#     staff.tanggal_lahir = datetime.datetime.strptime(
#         datas[15], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[15] not in ['', None] else None
#     staff.alamat_sekarang = datas[16]
#     staff.no_ktp = datas[17]
#     staff.email_bas = datas[18]
#     staff.email_pribadi = datas[19]
#     # staff.kondisi = datas[20]
#     staff.referensi = datas[20]
#     staff.status = datas[21]
#     staff.no_telpon = datas[22]
#     staff.nik = datas[23]
#     staff.nama_istri = datas[24]
#     staff.nama_anak1 = datas[25]
#     staff.nama_anak2 = datas[26]
#     staff.nama_anak3 = datas[27]
#     staff.bpjs_kesehatan = datas[28]
#     staff.masa_bpjs_kesehatan = datas[29]
#     staff.bpjs_tenaga_kerja = datas[30]
#     staff.masa_bpjs_tenaga_kerja = datas[31]
#     staff.no_polis = datas[32]
#     staff.pendidikan_terakhir = datas[33]
#     staff.sertifikat1 = datas[34]
#     staff.sertifikat2 = datas[35]
#     staff.sertifikat3 = datas[36]
#     staff.sertifikat4 = datas[37]
#     staff.training_diikuti = datas[38]
#     staff.training1 = datetime.datetime.strptime(
#         datas[39], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[39] not in ['', None] else None
#     staff.training2 = datetime.datetime.strptime(
#         datas[40], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[40] not in ['', None] else None
#     staff.training3 = datetime.datetime.strptime(
#         datas[41], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[41] not in ['', None] else None
#     staff.training4 = datetime.datetime.strptime(
#         datas[42], '%d-%b-%Y').strftime('%Y-%m-%d') if datas[42] not in ['', None] else None
#     staff.kelamin = datas[43]
#     staff.save()
#     history = History(
#         status="[EDIT STAFF] %s -  %s" % (staff.nopeg, staff.nama_pegawai)
#     )

#     history.save()


# def edit_payroll(datas, pk):
#     payroll = Payroll.objects.get(pk=pk)
#     staff = payroll.staff
#     bulan = payroll.bulan
#     status = payroll.status
#     payroll.delete()

#     edit_payroll = Payroll(
#         staff=staff,
#         gaji_pokok=datas[0],
#         tunjangan_jabatan=datas[1],
#         tunjangan_kehadiran_osa=datas[2],
#         tunjangan_lisensi=datas[3],
#         tunjangan_lainnya=datas[4],
#         lembur=datas[5],
#         tunjangan_pph_21=datas[6],
#         total_penghasilan=datas[7],
#         bpjs_kesehatan=datas[8],
#         bpjs_ketenagakerjaan=datas[9],
#         jaminan_pensiun=datas[10],
#         pot_pinjaman_1=datas[11],
#         pot_pinjaman_2=datas[12],
#         pot_pinjaman_3=datas[13],
#         simpanan_pokok_koperasi=datas[14],
#         simpanan_wajib_koperasi=datas[15],
#         pot_lainnya=datas[16],
#         pph_21=datas[17],
#         total_potongan=datas[18],
#         gaji_bersih=datas[19],
#         akumulasi_simpanan_pokok=datas[20],
#         akumulasi_simpanan_wajib=datas[21],
#         sisa_hutang_pinjaman=datas[22],
#         nomor_rekening=datas[23],
#         bulan=bulan,
#         status=status
#     )

#     edit_payroll.save()

#     history = History(
#         status="[EDIT PAYROLL] %s - %s" % (
#             edit_payroll.staff.nama_pegawai, edit_payroll.bulan)
#     )

#     history.save()


def payroll_sum(bulanan, thr, bonus):
    if bulanan == None:
        bulanan = 0
    if thr == None:
        thr = 0
    if bonus == None:
        bonus = 0

    return bulanan + thr + bonus


def check_obj_staff(nopeg):
    return Staff.objects.filter(nopeg=nopeg).exists()


def check_obj_payroll(nopeg, month, tipe):
    return Payroll.objects.filter(staff__nopeg=nopeg, bulan=month, tipe=tipe).exists()


def check_payroll_published(month, tipe):
    sample = Payroll.objects.filter(bulan=month, tipe=tipe)
    return (sample[0].status == "Published")


def get_months(now=datetime.datetime.now(), x=8):
    months = list()

    five_years_ago = 5 * 12
    this_years = (12 - now.month) + 1

    for i in range(1, five_years_ago):
        month = now - relativedelta(months=+i)
        months.append(month.strftime("%B %Y"))

    months.reverse()

    for i in range(this_years):
        month = now + relativedelta(months=+i)
        months.append(month.strftime("%B %Y"))

    months.reverse()

    return months


def convert_to_datetime(date):
    try:
        x = datetime.datetime.strptime(date, "%B %Y")
    except Exception as e:
        x = None
    return x


def get_month_list(start, end):
    try:
        start = datetime.datetime.strptime(start, "%B %Y")
    except:
        pass

    try:
        end = datetime.datetime.strptime(end, "%B %Y")
    except:
        pass

    months = list()

    try:
        x = get_months_delta(start, end)
        for i in range(x+1):
            month = end - relativedelta(months=+i)
            months.append(month.strftime("%B %Y"))

    except:
        pass

    return months


def get_min_max_month(query):
    min = None
    max = None

    for q in query:
        try:
            bulan = q['bulan']
        except:
            bulan = q['date']

        bulan = datetime.datetime.strptime(bulan, "%B %Y")

        if min == None or max == None:
            min = max = bulan
        else:
            if bulan < min:
                min = bulan
            elif bulan > max:
                max = bulan

    return min, max


def get_months_delta(start, end):
    return (end.year - start.year) * 12 + end.month - start.month


def get_all_staff_all_unit(bulan, field):
    if (isinstance(bulan, str)):
        bulan = [bulan]
    res = Dashboard.objects.filter(date__in=bulan).annotate(
        result=Sum(field)).aggregate(Sum('result'))
    return (res['result__sum'] if res['result__sum'] is not None else 0)


def get_all_staff_by_unit(unit, bulan, field):
    if (isinstance(bulan, str)):
        bulan = [bulan]
    res = Dashboard.objects.filter(date__in=bulan, unit=unit).annotate(
        result=Sum(field)).aggregate(Sum('result'))
    return (res['result__sum'] if res['result__sum'] is not None else 0)


def get_payroll_all_unit(bulan, field):
    if (isinstance(bulan, str)):
        bulan = [bulan]
    res = (Payroll.objects.filter(bulan__in=bulan
                                  ).annotate(as_float=Cast(field, FloatField())
                                             ).aggregate(Sum('as_float')))
    return (int(res['as_float__sum']) if res['as_float__sum'] is not None else 0)


def get_payroll_by_unit(dept, bulan, field):
    if (isinstance(bulan, str)):
        bulan = [bulan]
    res = (Payroll.objects.filter(staff__departemen_sekarang__contains=dept, bulan__in=bulan
                                  ).annotate(as_float=Cast(field, FloatField())
                                             ).aggregate(Sum('as_float')))
    return (int(res['as_float__sum']) if res['as_float__sum'] is not None else 0)


def get_obj_staff(nopeg):
    rows = StatusStaff.objects.filter(staff__nopeg=nopeg).values_list(
        "staff__nopeg",
        "staff__nama_pegawai",
        "staff__npwp",
        "staff__nik",
        "staff__basis_gaji",
        "staff__pph_kembali_bulan_terakhir",
        "staff__jenis_kelamin",
        "staff__status_pajak",
        "staff__status_kepegawaian",
        "staff__dept_code",
        "staff__gl_account",
        "staff__group_expenses",
        "staff__dept",
        "staff__golongan",
        "staff__jabatan",
        "staff__tanggal_mulai_kerja",
        "status_aktif",
        "staff__mulai_payslip",
        "staff__berakhir_payslip",
        "keterangan_aktif",
        "staff__nomor_kontrak",
        "staff__masa_kontrak",
        "status_tidak_aktif",
        "finish_contract",
        "pengganti",
        "keterangan_tidak_aktif",
        "staff__ditempatkan_di_perusahaan",
        "staff__under_supervisi",
        "staff__lokasi",
        "staff__nomor_id_card",
        "staff__masa_berlaku_id_card",
        "staff__tempat_lahir",
        "staff__tanggal_lahir",
        "staff__alamat_domisili_sekarang",
        "staff__alamat_email_perusahaan",
        "staff__alamat_email_pribadi",
        "staff__nomor_telpon",
        "staff__nomor_kartu_keluarga",
        "staff__nama_suami_istri",
        "staff__nama_anak_satu",
        "staff__nama_anak_dua",
        "staff__nama_anak_tiga",
        "staff__nomor_bpjs_kesehatan",
        "staff__masa_berlaku_bpjs_kesehatan",
        "staff__nomor_bpjs_tenagakerja",
        "staff__masa_berlaku_bpjs_tenagakerja",
        "staff__premi_asuransi",
        "staff__pendidikan_terakhir",
        "staff__sertifikat_satu",
        "staff__sertifikat_dua",
        "staff__sertifikat_tiga",
        "staff__sertifikat_empat",
        "staff__training_diikuti",
        "staff__training_satu",
        "staff__training_dua",
        "staff__training_tiga",
        "staff__training_empat",
        "staff__referensi",
    )

    return rows[0]


def get_payroll_dashboard(month):
    response = {}
    tunjangan_jabatan = get_payroll_all_unit(month, 'tunjangan_jabatan') if get_payroll_all_unit(
        month, 'tunjangan_jabatan') is not None else 0

    return response


def get_payroll_dashboard_unit(month, unit):
    response = {}
    tunjangan_jabatan = get_payroll_by_unit(unit, month, 'tunjangan_jabatan') if get_payroll_by_unit(
        unit, month, 'tunjangan_jabatan') is not None else 0
    tunjangan_kehadiran_osa = get_payroll_by_unit(unit, month, 'tunjangan_kehadiran_osa') if get_payroll_by_unit(
        unit, month, 'tunjangan_kehadiran_osa') is not None else 0
    tunjangan_lisensi = get_payroll_by_unit(unit, month, 'tunjangan_lisensi') if get_payroll_by_unit(
        unit, month, 'tunjangan_lisensi') is not None else 0
    tunjangan_lainnya = get_payroll_by_unit(unit, month, 'tunjangan_lainnya') if get_payroll_by_unit(
        unit, month, 'tunjangan_lainnya') is not None else 0

    pot_pinjaman_1 = get_payroll_by_unit(unit, month, 'pot_pinjaman_1') if get_payroll_by_unit(
        unit, month, 'pot_pinjaman_1') is not None else 0
    pot_pinjaman_2 = get_payroll_by_unit(unit, month, 'pot_pinjaman_2') if get_payroll_by_unit(
        unit, month, 'pot_pinjaman_2') is not None else 0
    pot_pinjaman_3 = get_payroll_by_unit(unit, month, 'pot_pinjaman_3') if get_payroll_by_unit(
        unit, month, 'pot_pinjaman_3') is not None else 0

    response['gaji_pokok'] = get_payroll_by_unit(unit, month, 'gaji_pokok') if get_payroll_by_unit(
        unit, month, 'gaji_pokok') is not None else 0
    response['total_penghasilan'] = get_payroll_by_unit(unit, month, 'total_penghasilan') if get_payroll_by_unit(
        unit, month, 'total_penghasilan') is not None else 0
    response['total_pinjaman'] = pot_pinjaman_1 + \
        pot_pinjaman_2 + pot_pinjaman_3
    response['pph_21'] = get_payroll_by_unit(unit, month, 'pph_21') if get_payroll_by_unit(
        unit, month, 'pph_21') is not None else 0
    response['akumulasi_simpanan_wajib'] = get_payroll_by_unit(unit, month, 'akumulasi_simpanan_wajib') if get_payroll_by_unit(
        unit, month, 'akumulasi_simpanan_wajib') is not None else 0

    response['total_tunjangan'] = tunjangan_jabatan + \
        tunjangan_kehadiran_osa + tunjangan_lainnya + tunjangan_lisensi
    response['bpjs_kesehatan'] = get_payroll_by_unit(unit, month, 'bpjs_kesehatan') if get_payroll_by_unit(
        unit, month, 'bpjs_kesehatan') is not None else 0
    response['simpanan_pokok_koperasi'] = get_payroll_by_unit(unit, month, 'simpanan_pokok_koperasi') if get_payroll_by_unit(
        unit, month, 'simpanan_pokok_koperasi') is not None else 0
    response['total_potongan'] = get_payroll_by_unit(unit, month, 'total_potongan') if get_payroll_by_unit(
        unit, month, 'total_potongan') is not None else 0
    response['sisa_hutang_pinjaman'] = get_payroll_by_unit(unit, month, 'sisa_hutang_pinjaman') if get_payroll_by_unit(
        unit, month, 'sisa_hutang_pinjaman') is not None else 0

    response['lembur'] = get_payroll_by_unit(unit, month, 'lembur') if get_payroll_by_unit(
        unit, month, 'lembur') is not None else 0
    response['bpjs_ketenagakerjaan'] = get_payroll_by_unit(unit, month, 'bpjs_ketenagakerjaan') if get_payroll_by_unit(
        unit, month, 'bpjs_ketenagakerjaan') is not None else 0
    response['gaji_bersih'] = get_payroll_by_unit(unit, month, 'gaji_bersih') if get_payroll_by_unit(
        unit, month, 'gaji_bersih') is not None else 0
    response['simpanan_wajib_koperasi'] = get_payroll_by_unit(unit, month, 'simpanan_wajib_koperasi') if get_payroll_by_unit(
        unit, month, 'simpanan_wajib_koperasi') is not None else 0

    response['tunjangan_pph_21'] = get_payroll_by_unit(unit, month, 'tunjangan_pph_21') if get_payroll_by_unit(
        unit, month, 'tunjangan_pph_21') is not None else 0
    response['jaminan_pensiun'] = get_payroll_by_unit(unit, month, 'jaminan_pensiun') if get_payroll_by_unit(
        unit, month, 'jaminan_pensiun') is not None else 0
    response['akumulasi_simpanan_pokok'] = get_payroll_by_unit(unit, month, 'akumulasi_simpanan_pokok') if get_payroll_by_unit(
        unit, month, 'akumulasi_simpanan_pokok') is not None else 0
    response['pot_lainnya'] = get_payroll_by_unit(unit, month, 'pot_lainnya') if get_payroll_by_unit(
        unit, month, 'pot_lainnya') is not None else 0
    return response
