from ui.models import *
from ui.constant import *

import datetime


def save_obj_staff(data, method='xlsx'):

    if method == 'xlsx':
        data = [d.value for d in data]
    else:
        data.insert(0, None)

    try:
        staff = Staff(
            username=data[nopeg],
            password=Staff.objects.make_random_password(8),
            nopeg=data[nopeg],
            nama_pegawai=data[nama_pegawai],
            npwp=data[npwp],
            nik=data[nik],
            basis_gaji=data[basis_gaji],
            pph_kembali_bulan_terakhir=data[pph_kembali_bulan_terakhir],
            jenis_kelamin=data[jenis_kelamin],
            status_pajak=data[status_pajak],
            status_kepegawaian=data[status_kepegawaian],
            dept_code=data[dept_code],
            gl_account=data[gl_account],
            group_expenses=data[group_expenses],
            dept=data[dept],
            golongan=data[golongan],
            jabatan=data[jabatan],
            tanggal_mulai_kerja=convert_to_date(
                data[tanggal_mulai_kerja]),
            mulai_payslip=convert_to_date(data[mulai_payslip]),
            berakhir_payslip=convert_to_date(data[berakhir_payslip]),
            nomor_kontrak=data[nomor_kontrak],
            masa_kontrak=data[masa_kontrak],
            ditempatkan_di_perusahaan=data[ditempatkan_di_perusahaan],
            under_supervisi=data[under_supervisi],
            lokasi=data[lokasi],
            nomor_id_card=data[nomor_id_card],
            masa_berlaku_id_card=convert_to_date(
                data[masa_berlaku_id_card]),
            tempat_lahir=data[tempat_lahir],
            tanggal_lahir=convert_to_date(data[tanggal_lahir]),
            alamat_domisili_sekarang=data[alamat_domisili_sekarang],
            alamat_email_perusahaan=data[alamat_email_perusahaan],
            alamat_email_pribadi=data[alamat_email_pribadi],
            nomor_telpon=data[nomor_telpon],
            nomor_kartu_keluarga=data[nomor_kartu_keluarga],
            nama_suami_istri=data[nama_suami_istri],
            nama_anak_satu=data[nama_anak_satu],
            nama_anak_dua=data[nama_anak_dua],
            nama_anak_tiga=data[nama_anak_tiga],
            nomor_bpjs_kesehatan=data[nomor_bpjs_kesehatan],
            masa_berlaku_bpjs_kesehatan=convert_to_date(
                data[masa_berlaku_bpjs_kesehatan]),
            nomor_bpjs_tenagakerja=data[nomor_bpjs_tenagakerja],
            masa_berlaku_bpjs_tenagakerja=convert_to_date(
                data[masa_berlaku_bpjs_tenagakerja]),
            premi_asuransi=data[premi_asuransi],
            pendidikan_terakhir=data[pendidikan_terakhir],
            sertifikat_satu=data[sertifikat_satu],
            sertifikat_dua=data[sertifikat_dua],
            sertifikat_tiga=data[sertifikat_tiga],
            sertifikat_empat=data[sertifikat_empat],
            training_diikuti=data[training_diikuti],
            training_satu=data[training_satu],
            training_dua=data[training_dua],
            training_tiga=data[training_tiga],
            training_empat=data[training_empat],
            referensi=data[referensi],
        )

        staff.save()

    except Exception as e:
        print(e)
        print('error save staff')

    try:
        staff_foreignkey = Staff.objects.get(nopeg=data[nopeg])

        status_staff = StatusStaff(
            staff=staff_foreignkey,
            status_pegawai=staff_status(
                staff_foreignkey.status_kepegawaian),
            status_aktif=data[status_aktif],
            keterangan_aktif=data[keterangan_aktif],
            status_tidak_aktif=data[status_tidak_aktif],
            finish_contract=data[finish_contract],
            pengganti=data[pengganti],
            keterangan_tidak_aktif=data[keterangan_tidak_aktif],
        )

        status_staff.save()

    except:
        print('error save status staff')


def update_obj_staff(data, method='xlsx'):

    # for constant.py purpose only
    data.insert(0, None)

    try:
        staff = Staff.objects.get(nopeg=data[nopeg])
        staff.nama_pegawai = data[nama_pegawai]
        staff.npwp = data[npwp]
        staff.nik = data[nik]
        staff.basis_gaji = data[basis_gaji]
        staff.pph_kembali_bulan_terakhir = data[pph_kembali_bulan_terakhir]
        staff.jenis_kelamin = data[jenis_kelamin]
        staff.status_pajak = data[status_pajak]
        staff.status_kepegawaian = data[status_kepegawaian]
        staff.dept_code = data[dept_code]
        staff.gl_account = data[gl_account]
        staff.group_expenses = data[group_expenses]
        staff.dept = data[dept]
        staff.golongan = data[golongan]
        staff.jabatan = data[jabatan]
        staff.tanggal_mulai_kerja = convert_to_date(data[tanggal_mulai_kerja])
        staff.mulai_payslip = convert_to_date(data[mulai_payslip])
        staff.berakhir_payslip = convert_to_date(data[berakhir_payslip])
        staff.nomor_kontrak = data[nomor_kontrak]
        staff.masa_kontrak = data[masa_kontrak]
        staff.ditempatkan_di_perusahaan = data[ditempatkan_di_perusahaan]
        staff.under_supervisi = data[under_supervisi]
        staff.lokasi = data[lokasi]
        staff.nomor_id_card = data[nomor_id_card]
        staff.masa_berlaku_id_card = convert_to_date(
            data[masa_berlaku_id_card])
        staff.tempat_lahir = data[tempat_lahir]
        staff.tanggal_lahir = convert_to_date(data[tanggal_lahir])
        staff.alamat_domisili_sekarang = data[alamat_domisili_sekarang]
        staff.alamat_email_perusahaan = data[alamat_email_perusahaan]
        staff.alamat_email_pribadi = data[alamat_email_pribadi]
        staff.nomor_telpon = data[nomor_telpon]
        staff.nomor_kartu_keluarga = data[nomor_kartu_keluarga]
        staff.nama_suami_istri = data[nama_suami_istri]
        staff.nama_anak_satu = data[nama_anak_satu]
        staff.nama_anak_dua = data[nama_anak_dua]
        staff.nama_anak_tiga = data[nama_anak_tiga]
        staff.nomor_bpjs_kesehatan = data[nomor_bpjs_kesehatan]
        staff.masa_berlaku_bpjs_kesehatan = convert_to_date(
            data[masa_berlaku_bpjs_kesehatan])
        staff.nomor_bpjs_tenagakerja = data[nomor_bpjs_tenagakerja]
        staff.masa_berlaku_bpjs_tenagakerja = convert_to_date(
            data[masa_berlaku_bpjs_tenagakerja])
        staff.premi_asuransi = data[premi_asuransi]
        staff.pendidikan_terakhir = data[pendidikan_terakhir]
        staff.sertifikat_satu = data[sertifikat_satu]
        staff.sertifikat_dua = data[sertifikat_dua]
        staff.sertifikat_tiga = data[sertifikat_tiga]
        staff.sertifikat_empat = data[sertifikat_empat]
        staff.training_diikuti = data[training_diikuti]
        staff.training_satu = data[training_satu]
        staff.training_dua = data[training_dua]
        staff.training_tiga = data[training_tiga]
        staff.training_empat = data[training_empat]
        staff.referensi = data[referensi]

        staff.save()
    except Exception as e:
        print(e)
        print('error update staff')

    if method == 'xlsx':
        try:
            status_staff = StatusStaff.objects.get(staff=staff)
            status_staff.status_pegawai = staff_status(
                staff.status_kepegawaian)
            status_staff.status_aktif = data[status_aktif]
            status_staff.keterangan_aktif = data[keterangan_aktif]
            status_staff.status_tidak_aktif = data[status_tidak_aktif]
            status_staff.finish_contract = data[finish_contract]
            status_staff.pengganti = data[pengganti]
            status_staff.keterangan_tidak_aktif = data[keterangan_tidak_aktif]

            status_staff.save()
        except Exception as e:
            print('error update status staff')


def status_staff_form(datas, action='edit'):
    staff = Staff.objects.get(nopeg=datas[0])
    staff.status_kepegawaian = datas[1]
    staff.save()

    if action == 'edit':
        statusStaff = StatusStaff.objects.get(staff=staff)
    else:
        statusStaff = StatusStaff(staff=staff)
        statusStaff.save()

    if datas[1] == 'aktif':
        statusStaff.status_pegawai = datas[1]
        statusStaff.status_aktif = datas[2]
        statusStaff.keterangan_aktif = datas[3]
        statusStaff.status_tidak_aktif = None
        statusStaff.finish_contract = None
        statusStaff.pengganti = None
        statusStaff.keterangan_tidak_aktif = None

    else:
        statusStaff.status_pegawai = datas[1]
        statusStaff.status_aktif = None
        statusStaff.keterangan_aktif = None
        statusStaff.status_tidak_aktif = datas[2]
        statusStaff.finish_contract = datas[3]
        statusStaff.pengganti = datas[4]
        statusStaff.keterangan_tidak_aktif = datas[5]

    statusStaff.save()


def staff_status(status):
    if status == 'aktif':
        return 'aktif'
    else:
        return 'nonaktif'


def convert_to_int(data):
    return int(data) if data not in ['', None] else None


def convert_to_date(data):
    try:
        if type(data) == datetime.datetime:
            return data

        if 'T' in data:
            formats = '%Y-%m-%dT%H:%M:%S'
        else:
            formats = '%Y-%m-%d'

        return datetime.datetime\
            .strptime(data, formats)\
            .strftime('%Y-%m-%d') if data not in ['', None] else None

    except Exception as e:
        return None
