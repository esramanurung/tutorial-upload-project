from ui.models import *
from ui.constant import *
from ui.constant_2 import *
from ui.tax import *


def save_obj_payroll(data, month, tipe, method='xlsx'):

    if method == 'xlsx':
        data = [d.value for d in data]
    else:
        data = ['0' if d == '' else d for d in data]
        data.insert(0, None)

    try:
        staff = Staff.objects.get(nopeg=data[nopeg])

        tanggal = datetime.datetime.strptime(month, "%B %Y")
        bulan_sebelum_x = get_month_year(tanggal)
        range_bulan, durasi_pph_21 = get_month_range(
            staff.mulai_payslip, staff.berakhir_payslip)

        recap = (len(bulan_sebelum_x) == 11) or (
            staff.pph_kembali_bulan_terakhir == 'aktif')

        v_gaji_pokok = to_float(data[gaji_pokok])
        v_lembur = to_float(data[lembur])
        v_thr = to_float(data[thr])
        v_rapel = to_float(data[rapel])
        v_bonus = to_float(data[bonus])
        v_uang_makan = to_float(data[uang_makan])
        v_tunjangan_jabatan = to_float(data[tunjangan_jabatan])
        v_seniority = to_float(data[seniority])
        v_transport = to_float(data[transport])
        v_tunjangan_keluarga = to_float(data[tunjangan_keluarga])
        v_tunjangan_shift = to_float(data[tunjangan_shift])
        v_tunjangan_kehadiran = to_float(data[tunjangan_kehadiran])
        v_tunjangan_lainnya = to_float(data[tunjangan_lainnya])

        v_potongan_absen = to_float(data[potongan_absen])
        v_potongan_lainnya = to_float(data[potongan_lainnya])

        v_astek_jp = to_float(data[astek_jp])
        v_bpjs_kesehatan_1 = to_float(data[bpjs_kesehatan_1])
        v_bpjs_kesehatan_4 = to_float(data[bpjs_kesehatan_4])
        v_asuransi_jkk_jkm = to_float(data[asuransi_jkk_jkm])
        v_iuran_spsi = to_float(data[iuran_spsi])

        v_gaji_netto_sebelum = to_float(data[gaji_netto_sebelum])
        v_pph_21_sebelum = to_float(data[pph_21_sebelum])

        if recap:
            v_gaji_pokok += get_gaji_pokok_pertahun(
                staff.nopeg, range_bulan)
            v_thr += get_thr_pertahun(staff.nopeg, range_bulan)
            v_bonus += get_bonus_pertahun(staff.nopeg, range_bulan)
            v_astek_jp += get_iuran_pensiun_pertahun(
                staff.nopeg, range_bulan)
            v_gaji_netto_sebelum += get_gaji_netto_sebelum_pertahun(
                staff.nopeg, range_bulan)
            v_pph_21_sebelum += get_pph_21_sebelum_pertahun(
                staff.nopeg, range_bulan)

        v_tax_tunjangan_lain = get_tunjangan_lain(v_lembur, v_rapel, v_uang_makan, v_tunjangan_jabatan,
                                                  v_seniority, v_transport, v_tunjangan_keluarga,
                                                  v_tunjangan_shift, v_tunjangan_kehadiran,
                                                  v_tunjangan_lainnya, v_potongan_absen, v_potongan_lainnya,
                                                  recap=recap, nopeg=staff.nopeg, bulan=range_bulan
                                                  )

        v_tax_asuransi = get_asuransi(v_bpjs_kesehatan_4, v_asuransi_jkk_jkm,
                                      recap=recap, nopeg=staff.nopeg, bulan=range_bulan
                                      )

        v_tax_penghasilan_bruto_reg = get_penghasilan_bruto_reg(
            v_gaji_pokok, v_tax_tunjangan_lain, v_tax_asuransi)

        v_tax_penghasilan_bruto_irreg = get_penghasilan_bruto_irreg(
            v_thr, v_bonus
        )

        v_tax_biaya_jabatan_reg = get_biaya_jabatan_reg(
            v_tax_penghasilan_bruto_reg, durasi_pph_21, recap=recap)

        v_tax_biaya_jabatan_irreg = get_biaya_jabatan_irreg(
            v_tax_biaya_jabatan_reg, v_tax_penghasilan_bruto_irreg, durasi_pph_21, recap=recap
        )

        v_tax_penghasilan_netto_perbulan_reg = get_penghasilan_netto_perbulan_reg(
            v_astek_jp, v_tax_penghasilan_bruto_reg, v_tax_biaya_jabatan_reg
        )

        v_tax_penghasilan_netto_bulan_sebelum_x = get_penghasilan_netto_bulan_sebelum_x(
            staff.nopeg, bulan_sebelum_x)

        v_tax_penghasilan_netto_pertahun_reg = get_penghasilan_netto_pertahun_reg(
            tanggal.month, v_tax_penghasilan_netto_perbulan_reg, v_tax_penghasilan_netto_bulan_sebelum_x, recap=recap
        )

        v_tax_penghasilan_netto_pertahun_irreg = get_penghasilan_netto_pertahun_irreg(
            v_tax_penghasilan_bruto_irreg, v_tax_biaya_jabatan_irreg
        )

        v_tax_ptkp = get_ptkp(staff.status_pajak)

        v_tax_pkp_reg = get_pkp_reg(
            v_tax_penghasilan_netto_pertahun_reg, v_gaji_netto_sebelum, v_tax_ptkp
        )

        v_tax_pkp_reg_irreg = get_pkp_reg_irreg(
            v_tax_pkp_reg, v_tax_penghasilan_netto_pertahun_irreg, v_tax_ptkp, v_tax_penghasilan_netto_perbulan_reg, v_gaji_netto_sebelum,
        )

        v_tax_pph_21_pertahun_reg_irreg = get_pph_21_pertahun_reg_irreg(
            staff.npwp, v_tax_pkp_reg_irreg, staff.basis_gaji
        )

        v_tax_pph_21_pertahun_reg = get_pph_21_pertahun_reg(
            staff.npwp, v_tax_pkp_reg, staff.basis_gaji
        )

        v_tax_pph_21_pertahun_irreg = get_pph_21_pertahun_irreg(
            v_tax_pph_21_pertahun_reg_irreg, v_tax_pph_21_pertahun_reg
        )

        v_tax_pph_21_pertahun_all = get_pph_21_pertahun_all(
            v_tax_pph_21_pertahun_reg_irreg, v_pph_21_sebelum
        )

        v_tax_pph_21_perbulan_all = get_pph_21_perbulan_all(
            v_tax_pph_21_pertahun_reg, v_tax_pph_21_pertahun_irreg, v_pph_21_sebelum, recap=recap
        )

        v_tax_pph_21_sistem_fixed_sebelum_x = get_pph_21_sistem_fixed_sebelum_x(
            staff.nopeg, bulan_sebelum_x
        )

        v_tax_adjustment_pph_21 = get_adjustment_pph_21(
            v_tax_pph_21_pertahun_all, v_tax_pph_21_sistem_fixed_sebelum_x, recap=recap
        )

        v_tax_pph_21_perbulan_sistem = get_pph_21_perbulan_sistem(
            v_tax_pph_21_perbulan_all, v_tax_adjustment_pph_21
        )

        v_tax_pph_21_perbulan_manual = to_float(data[pph_21])

        if v_tax_pph_21_perbulan_manual > 0:
            v_tax_pph_21_perbulan_fixed = v_tax_pph_21_perbulan_manual
            v_penghitung_pph_21 = 'dipilih'
        else:
            v_tax_pph_21_perbulan_fixed = v_tax_pph_21_perbulan_sistem
            v_penghitung_pph_21 = 'sistem'

        if staff.basis_gaji == 'Gross':
            v_tunjangan_pph_21 = 0
        else:
            v_tunjangan_pph_21 = v_tax_pph_21_perbulan_fixed

        v_gaji_bruto = get_gaji_bruto(to_float(data[gaji_pokok]), v_potongan_absen, v_lembur, to_float(data[thr]), v_rapel,
                                      v_bonus, v_uang_makan, v_tunjangan_jabatan, v_seniority, v_transport,
                                      v_tunjangan_keluarga, v_tunjangan_shift, v_tunjangan_kehadiran,
                                      v_tunjangan_lainnya, v_tunjangan_pph_21, v_bpjs_kesehatan_4,
                                      v_asuransi_jkk_jkm)

        v_salary_exclude_ot = get_salary_exclude_ot(v_gaji_bruto, v_lembur, to_float(data[astek_jp]),
                                                    v_bpjs_kesehatan_1, v_bpjs_kesehatan_4, v_asuransi_jkk_jkm)

        v_total_potongan = get_total_potongan(v_tax_pph_21_perbulan_fixed, to_float(data[astek_jp]), v_bpjs_kesehatan_1,
                                              v_bpjs_kesehatan_4, v_asuransi_jkk_jkm,
                                              v_iuran_spsi, v_potongan_lainnya)

        v_gaji_netto = get_gaji_netto(v_gaji_bruto, v_total_potongan)

        payroll = Payroll(
            staff=staff,
            bulan=month,
            tipe=tipe,
            gaji_pokok=to_float(data[gaji_pokok]),
            potongan_absen=v_potongan_absen,
            lembur=v_lembur,
            thr=to_float(data[thr]),
            rapel=v_rapel,
            bonus=to_float(data[bonus]),
            uang_makan=v_uang_makan,
            tunjangan_jabatan=v_tunjangan_jabatan,
            seniority=v_seniority,
            transport=v_transport,
            tunjangan_keluarga=v_tunjangan_keluarga,
            tunjangan_shift=v_tunjangan_shift,
            tunjangan_kehadiran=v_tunjangan_kehadiran,
            tunjangan_lainnya=v_tunjangan_lainnya,
            tunjangan_pph_21=v_tunjangan_pph_21,
            gaji_bruto=v_gaji_bruto,
            salary_exclude_ot=v_salary_exclude_ot,
            pph_21=v_tax_pph_21_perbulan_fixed,
            astek_jp=to_float(data[astek_jp]),
            bpjs_kesehatan_1=v_bpjs_kesehatan_1,
            bpjs_kesehatan_4=v_bpjs_kesehatan_4,
            asuransi_jkk_jkm=v_asuransi_jkk_jkm,
            iuran_spsi=v_iuran_spsi,
            potongan_lainnya=v_potongan_lainnya,
            total_potongan=v_total_potongan,
            gaji_netto=v_gaji_netto,
            nomor_rekening=data[nomor_rekening],
            bank=data[bank],

            gaji_netto_sebelum=to_float(data[gaji_netto_sebelum]),
            pph_21_sebelum=to_float(data[pph_21_sebelum]),

            tax_gaji_pokok=v_gaji_pokok,
            tax_thr=v_thr,
            tax_bonus=v_bonus,
            tax_iuran_pensiun=v_astek_jp,
            tax_gaji_netto_sebelum=v_gaji_netto_sebelum,
            tax_pph_21_sebelum=v_pph_21_sebelum,

            tax_tunjangan_lain=v_tax_tunjangan_lain,
            tax_asuransi=v_tax_asuransi,
            tax_penghasilan_bruto_reg=v_tax_penghasilan_bruto_reg,
            tax_penghasilan_bruto_irreg=v_tax_penghasilan_bruto_irreg,
            tax_biaya_jabatan_reg=v_tax_biaya_jabatan_reg,
            tax_biaya_jabatan_irreg=v_tax_biaya_jabatan_irreg,
            tax_penghasilan_netto_perbulan_reg=v_tax_penghasilan_netto_perbulan_reg,
            tax_penghasilan_netto_pertahun_reg=v_tax_penghasilan_netto_pertahun_reg,
            tax_penghasilan_netto_pertahun_irreg=v_tax_penghasilan_netto_pertahun_irreg,
            tax_ptkp=v_tax_ptkp,
            tax_pkp_reg=v_tax_pkp_reg,
            tax_pkp_reg_irreg=v_tax_pkp_reg_irreg,
            tax_pph_21_pertahun_reg_irreg=v_tax_pph_21_pertahun_reg_irreg,
            tax_pph_21_pertahun_reg=v_tax_pph_21_pertahun_reg,
            tax_pph_21_pertahun_irreg=v_tax_pph_21_pertahun_irreg,
            tax_pph_21_pertahun_all=v_tax_pph_21_pertahun_all,
            tax_pph_21_perbulan_all=v_tax_pph_21_perbulan_all,
            tax_adjustment_pph_21=v_tax_adjustment_pph_21,
            tax_total_pph_21_fixed_sebelum=v_tax_pph_21_sistem_fixed_sebelum_x,
            tax_pph_21_perbulan_sistem=v_tax_pph_21_perbulan_sistem,
            tax_pph_21_perbulan_manual=v_tax_pph_21_perbulan_manual,
            tax_pph_21_perbulan_fixed=v_tax_pph_21_perbulan_fixed,
            penghitung_pph_21=v_penghitung_pph_21,
        )

        payroll.save()

    except Exception as e:
        print(e)
        print('error save payroll')


def update_obj_payroll(data, month, tipe, method='xlsx', tax_manual=0, penghitung='sistem'):

    if method == 'xlsx':
        data = [d.value for d in data]
    else:

        # for constant.py purpose only
        data = ['0' if d == '' else d for d in data]
        data.insert(0, None)

    try:
        payroll = Payroll.objects.get(
            staff__nopeg=data[nopeg], bulan=month, tipe=tipe)

        tanggal = datetime.datetime.strptime(month, "%B %Y")
        bulan_sebelum_x = get_month_year(tanggal)
        range_bulan, durasi_pph_21 = get_month_range(
            payroll.staff.mulai_payslip, payroll.staff.berakhir_payslip)

        recap = (len(bulan_sebelum_x) == 11) or (
            payroll.staff.pph_kembali_bulan_terakhir == 'aktif')

        v_gaji_pokok = to_float(data[gaji_pokok])
        v_lembur = to_float(data[lembur])
        v_thr = to_float(data[thr])
        v_rapel = to_float(data[rapel])
        v_bonus = to_float(data[bonus])
        v_uang_makan = to_float(data[uang_makan])
        v_tunjangan_jabatan = to_float(data[tunjangan_jabatan])
        v_seniority = to_float(data[seniority])
        v_transport = to_float(data[transport])
        v_tunjangan_keluarga = to_float(data[tunjangan_keluarga])
        v_tunjangan_shift = to_float(data[tunjangan_shift])
        v_tunjangan_kehadiran = to_float(data[tunjangan_kehadiran])
        v_tunjangan_lainnya = to_float(data[tunjangan_lainnya])

        v_potongan_absen = to_float(data[potongan_absen])
        v_potongan_lainnya = to_float(data[potongan_lainnya])

        v_astek_jp = to_float(data[astek_jp])
        v_bpjs_kesehatan_1 = to_float(data[bpjs_kesehatan_1])
        v_bpjs_kesehatan_4 = to_float(data[bpjs_kesehatan_4])
        v_asuransi_jkk_jkm = to_float(data[asuransi_jkk_jkm])
        v_iuran_spsi = to_float(data[iuran_spsi])

        v_gaji_netto_sebelum = to_float(data[gaji_netto_sebelum])
        v_pph_21_sebelum = to_float(data[pph_21_sebelum])

        if recap:
            v_gaji_pokok += get_gaji_pokok_pertahun(
                payroll.nopeg, range_bulan)
            v_thr += get_thr_pertahun(payroll.staff.nopeg, range_bulan)
            v_bonus += get_bonus_pertahun(payroll.staff.nopeg, range_bulan)
            v_astek_jp += get_iuran_pensiun_pertahun(
                payroll.staff.nopeg, range_bulan)
            v_gaji_netto_sebelum += get_gaji_netto_sebelum_pertahun(
                payroll.staff.nopeg, range_bulan)
            v_pph_21_sebelum += get_pph_21_sebelum_pertahun(
                payroll.staff.nopeg, range_bulan)

        v_tax_tunjangan_lain = get_tunjangan_lain(v_lembur, v_rapel, v_uang_makan, v_tunjangan_jabatan,
                                                  v_seniority, v_transport, v_tunjangan_keluarga,
                                                  v_tunjangan_shift, v_tunjangan_kehadiran,
                                                  v_tunjangan_lainnya, v_potongan_absen, v_potongan_lainnya,
                                                  recap=recap, nopeg=payroll.staff.nopeg, bulan=range_bulan
                                                  )

        v_tax_asuransi = get_asuransi(v_bpjs_kesehatan_4, v_asuransi_jkk_jkm,
                                      recap=recap, nopeg=payroll.staff.nopeg, bulan=range_bulan
                                      )

        v_tax_penghasilan_bruto_reg = get_penghasilan_bruto_reg(
            v_gaji_pokok, v_tax_tunjangan_lain, v_tax_asuransi)

        v_tax_penghasilan_bruto_irreg = get_penghasilan_bruto_irreg(
            v_thr, v_bonus
        )

        v_tax_biaya_jabatan_reg = get_biaya_jabatan_reg(
            v_tax_penghasilan_bruto_reg, durasi_pph_21, recap=recap)

        v_tax_biaya_jabatan_irreg = get_biaya_jabatan_irreg(
            v_tax_biaya_jabatan_reg, v_tax_penghasilan_bruto_irreg, durasi_pph_21, recap=recap
        )

        v_tax_penghasilan_netto_perbulan_reg = get_penghasilan_netto_perbulan_reg(
            v_astek_jp, v_tax_penghasilan_bruto_reg, v_tax_biaya_jabatan_reg
        )

        v_tax_penghasilan_netto_bulan_sebelum_x = get_penghasilan_netto_bulan_sebelum_x(
            payroll.staff.nopeg, bulan_sebelum_x)

        v_tax_penghasilan_netto_pertahun_reg = get_penghasilan_netto_pertahun_reg(
            tanggal.month, v_tax_penghasilan_netto_perbulan_reg, v_tax_penghasilan_netto_bulan_sebelum_x, recap=recap
        )

        v_tax_penghasilan_netto_pertahun_irreg = get_penghasilan_netto_pertahun_irreg(
            v_tax_penghasilan_bruto_irreg, v_tax_biaya_jabatan_irreg
        )

        v_tax_ptkp = get_ptkp(payroll.staff.status_pajak)

        v_tax_pkp_reg = get_pkp_reg(
            v_tax_penghasilan_netto_pertahun_reg, v_gaji_netto_sebelum, v_tax_ptkp
        )

        v_tax_pkp_reg_irreg = get_pkp_reg_irreg(
            v_tax_pkp_reg, v_tax_penghasilan_netto_pertahun_irreg, v_tax_ptkp, v_tax_penghasilan_netto_perbulan_reg, v_gaji_netto_sebelum,
        )

        v_tax_pph_21_pertahun_reg_irreg = get_pph_21_pertahun_reg_irreg(
            payroll.staff.npwp, v_tax_pkp_reg_irreg, payroll.staff.basis_gaji
        )

        v_tax_pph_21_pertahun_reg = get_pph_21_pertahun_reg(
            payroll.staff.npwp, v_tax_pkp_reg, payroll.staff.basis_gaji
        )

        v_tax_pph_21_pertahun_irreg = get_pph_21_pertahun_irreg(
            v_tax_pph_21_pertahun_reg_irreg, v_tax_pph_21_pertahun_reg
        )

        v_tax_pph_21_pertahun_all = get_pph_21_pertahun_all(
            v_tax_pph_21_pertahun_reg_irreg, v_pph_21_sebelum
        )

        v_tax_pph_21_perbulan_all = get_pph_21_perbulan_all(
            v_tax_pph_21_pertahun_reg, v_tax_pph_21_pertahun_irreg, v_pph_21_sebelum, recap=recap
        )

        v_tax_pph_21_sistem_fixed_sebelum_x = get_pph_21_sistem_fixed_sebelum_x(
            payroll.staff.nopeg, bulan_sebelum_x
        )

        v_tax_adjustment_pph_21 = get_adjustment_pph_21(
            v_tax_pph_21_pertahun_all, v_tax_pph_21_sistem_fixed_sebelum_x, recap=recap
        )

        v_tax_pph_21_perbulan_sistem = get_pph_21_perbulan_sistem(
            v_tax_pph_21_perbulan_all, v_tax_adjustment_pph_21
        )

        v_tax_pph_21_perbulan_manual = to_float(data[pph_21])
        v_penghitung_pph_21 = penghitung

        if to_float(tax_manual) > 0:
            v_tax_pph_21_perbulan_manual = to_float(tax_manual)

        if v_penghitung_pph_21 == 'dipilih':
            v_tax_pph_21_perbulan_fixed = v_tax_pph_21_perbulan_manual
        else:
            v_tax_pph_21_perbulan_fixed = v_tax_pph_21_perbulan_sistem

        if payroll.staff.basis_gaji == 'Gross':
            v_tunjangan_pph_21 = 0
        else:
            v_tunjangan_pph_21 = v_tax_pph_21_perbulan_fixed

        v_gaji_bruto = get_gaji_bruto(v_gaji_pokok, v_potongan_absen, v_lembur, v_thr, v_rapel,
                                      v_bonus, v_uang_makan, v_tunjangan_jabatan, v_seniority, v_transport,
                                      v_tunjangan_keluarga, v_tunjangan_shift, v_tunjangan_kehadiran,
                                      v_tunjangan_lainnya, v_tunjangan_pph_21, v_bpjs_kesehatan_4,
                                      v_asuransi_jkk_jkm)

        v_salary_exclude_ot = get_salary_exclude_ot(v_gaji_bruto, v_lembur, v_astek_jp,
                                                    v_bpjs_kesehatan_1, v_bpjs_kesehatan_4, v_asuransi_jkk_jkm)

        v_total_potongan = get_total_potongan(v_tax_pph_21_perbulan_fixed, v_astek_jp, v_bpjs_kesehatan_1,
                                              v_bpjs_kesehatan_4, v_asuransi_jkk_jkm,
                                              v_iuran_spsi, v_potongan_lainnya)

        v_gaji_netto = get_gaji_netto(v_gaji_bruto, v_total_potongan)

        payroll.gaji_pokok = to_float(data[gaji_pokok])
        payroll.potongan_absen = v_potongan_absen
        payroll.lembur = v_lembur
        payroll.thr = to_float(data[thr])
        payroll.rapel = v_rapel
        payroll.bonus = to_float(data[bonus])
        payroll.uang_makan = v_uang_makan
        payroll.tunjangan_jabatan = v_tunjangan_jabatan
        payroll.seniority = v_seniority
        payroll.transport = v_transport
        payroll.tunjangan_keluarga = v_tunjangan_keluarga
        payroll.tunjangan_shift = v_tunjangan_shift
        payroll.tunjangan_kehadiran = v_tunjangan_kehadiran
        payroll.tunjangan_lainnya = v_tunjangan_lainnya
        payroll.tunjangan_pph_21 = v_tunjangan_pph_21
        payroll.gaji_bruto = v_gaji_bruto
        payroll.salary_exclude_ot = v_salary_exclude_ot
        payroll.pph_21 = v_tax_pph_21_perbulan_fixed
        payroll.astek_jp = to_float(data[astek_jp])
        payroll.bpjs_kesehatan_1 = v_bpjs_kesehatan_1
        payroll.bpjs_kesehatan_4 = v_bpjs_kesehatan_4
        payroll.asuransi_jkk_jkm = v_asuransi_jkk_jkm
        payroll.iuran_spsi = v_iuran_spsi
        payroll.potongan_lainnya = v_potongan_lainnya
        payroll.total_potongan = v_total_potongan
        payroll.gaji_netto = v_gaji_netto
        payroll.nomor_rekening = data[nomor_rekening]
        payroll.bank = data[bank]

        payroll.gaji_netto_sebelum = to_float(data[gaji_netto_sebelum])
        payroll.pph_21_sebelum = to_float(data[pph_21_sebelum])

        payroll.tax_gaji_pokok = v_gaji_pokok
        payroll.tax_thr = v_thr
        payroll.tax_bonus = v_bonus
        payroll.tax_iuran_pensiun = v_astek_jp
        payroll.tax_gaji_netto_sebelum = v_gaji_netto_sebelum
        payroll.tax_pph_21_sebelum = v_pph_21_sebelum

        payroll.tax_tunjangan_lain = v_tax_tunjangan_lain
        payroll.tax_asuransi = v_tax_asuransi
        payroll.tax_penghasilan_bruto_reg = v_tax_penghasilan_bruto_reg
        payroll.tax_penghasilan_bruto_irreg = v_tax_penghasilan_bruto_irreg
        payroll.tax_biaya_jabatan_reg = v_tax_biaya_jabatan_reg
        payroll.tax_biaya_jabatan_irreg = v_tax_biaya_jabatan_irreg
        payroll.tax_penghasilan_netto_perbulan_reg = v_tax_penghasilan_netto_perbulan_reg
        payroll.tax_penghasilan_netto_pertahun_reg = v_tax_penghasilan_netto_pertahun_reg
        payroll.tax_penghasilan_netto_pertahun_irreg = v_tax_penghasilan_netto_pertahun_irreg
        payroll.tax_ptkp = v_tax_ptkp
        payroll.tax_pkp_reg = v_tax_pkp_reg
        payroll.tax_pkp_reg_irreg = v_tax_pkp_reg_irreg
        payroll.tax_pph_21_pertahun_reg_irreg = v_tax_pph_21_pertahun_reg_irreg
        payroll.tax_pph_21_pertahun_reg = v_tax_pph_21_pertahun_reg
        payroll.tax_pph_21_pertahun_irreg = v_tax_pph_21_pertahun_irreg
        payroll.tax_pph_21_pertahun_all = v_tax_pph_21_pertahun_all
        payroll.tax_pph_21_perbulan_all = v_tax_pph_21_perbulan_all
        payroll.tax_adjustment_pph_21 = v_tax_adjustment_pph_21
        payroll.tax_total_pph_21_fixed_sebelum = v_tax_pph_21_sistem_fixed_sebelum_x
        payroll.tax_pph_21_perbulan_sistem = v_tax_pph_21_perbulan_sistem
        payroll.tax_pph_21_perbulan_manual = v_tax_pph_21_perbulan_manual
        payroll.tax_pph_21_perbulan_fixed = v_tax_pph_21_perbulan_fixed
        payroll.penghitung_pph_21 = v_penghitung_pph_21

        payroll.save()

    except Exception as e:
        print(e)
        print('error update payroll')


def get_gaji_bruto(gaji_pokok, potongan_absen, lembur, thr, rapel,
                   bonus, uang_makan, tunjangan_jabatan, seniority, transport,
                   tunjangan_keluarga, tunjangan_shift, tunjangan_kehadiran,
                   tunjangan_lainnya, tunjangan_pph_21, bpjs_kesehatan_4,
                   asuransi_jkk_jkm):

    return gaji_pokok \
        - potongan_absen \
        + lembur \
        + thr \
        + rapel \
        + bonus \
        + uang_makan \
        + tunjangan_jabatan \
        + seniority \
        + transport \
        + tunjangan_keluarga \
        + tunjangan_shift \
        + tunjangan_kehadiran \
        + tunjangan_lainnya \
        + tunjangan_pph_21 \
        + bpjs_kesehatan_4 \
        + asuransi_jkk_jkm


def get_salary_exclude_ot(gaji_bruto, lembur, astek_jp,
                          bpjs_kesehatan_1, bpjs_kesehatan_4, asuransi_jkk_jkm):
    return gaji_bruto \
        - lembur \
        - astek_jp \
        - bpjs_kesehatan_1 \
        - bpjs_kesehatan_4 \
        - asuransi_jkk_jkm \



def get_total_potongan(pph_21, astek_jp, bpjs_kesehatan_1,
                       bpjs_kesehatan_4, asuransi_jkk_jkm,
                       iuran_spsi, potongan_lainnya):
    return pph_21 \
        + astek_jp \
        + bpjs_kesehatan_1 \
        + bpjs_kesehatan_4 \
        + asuransi_jkk_jkm \
        + iuran_spsi \
        + potongan_lainnya


def get_gaji_netto(gaji_bruto, total_potongan):
    return gaji_bruto - total_potongan


def to_float(data):
    return float(data) if data != None else 0
