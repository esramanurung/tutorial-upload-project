from django.urls import path
from ui.views import *
from ui.tax import *

urlpatterns = [
    path('', homepage, name="homepage"),
    path('staff/', staff_index, name="staff_index"),
    path('staff/add/', staff_add, name="staff_add"),
    path('staff/edit/<nopeg>/', staff_edit, name="staff_edit"),
    path('staff/detail/<nopeg>/', staff_detail, name="staff_detail"),
    path('staff/upload/', staff_upload, name="staff_upload"),
    path('delete/staff/<nopeg>/', delete_staff, name="delete_staff"),
    path('add/staff/', add_staff_detail, name="add_staff_detail"),
    path('edit/detail/', edit_staff_detail, name="upload_detail"),
    path('upload/detail/', upload_staff_detail, name="upload_detail"),

    path('payroll/', payroll_index, name="payroll_index"),
    path('payroll/add/<bulan>/<tipe>/', payroll_add, name="payroll_add"),
    path('payroll/history/', payroll_history, name="payroll_history"),
    path('payroll/edit/<nopeg>/<bulan>/<tipe>/',
         payroll_edit, name="payroll_edit"),
    path('payroll/view/<filetype>/<nopeg>/<bulan>/<tipe>/',
         payroll_view, name="payroll_view"),
    path('payroll/upload/', payroll_upload, name="payroll_upload"),
    path('publish/payroll/<bulan>/<tipe>/',
         publish_payroll, name="publish_payroll"),
    path('add/payroll/', add_payroll_detail, name="add_payroll_detail"),
    path('edit/payroll/<pk>/', edit_payroll_detail, name="payroll_detail"),
    path('upload/payroll/', upload_payroll_detail, name="upload_payroll"),


    path('recalculate/tax/<bulan>/<tipe>/',
         recalculate_tax, name="recalculate_tax"),

    path('profile/', profile_index, name="profile_index"),
    path('profile/access/', profile_access, name="profile_acess"),
    path('profile/add/', add_staff_access, name="add_staff_access"),

    path('pdf/<nopeg>/<bulan>/<tipe>/', Pdf.as_view()),
    path('pdf/<bulan>/<tipe>/', BulkPdf.as_view()),
    path('pdf/staff/', StaffBulkPdf.as_view()),

    path('excel/<bulan>/<tipe>/', dot_matrix_format, name="dot_matrix"),

    path('export/', export_index, name="export_index"),
    path('export/payroll/<month>/<format>/',
         export_payroll_xlsx, name="export_payroll"),
    path('export/staff/', export_staff_xlsx, name="export_staff"),
    path('export/access/', export_access_xlsx, name="export_access"),

    # new iteration
    path('staff/edit-status/<nopeg>/',
         edit_status_form, name="edit_status_form"),
    path('edit-status/staff/', edit_status_detail, name="edit_status_detail"),
    path('staff/upload/edit', upload_staff_edit, name="upload_staff_edit"),
    path('profile/edit/<pk>', edit_staff_access, name="edit_staff_access"),
    path('edit/profile/<pk>', edit_staff_index, name="edit_staff_index"),
    path('delete/profile/<pk>', delete_staff_access,
         name="delete_staff_access"),
    path('dashboard/staff', dashboard_staff, name="dashboard_staff"),
    path('dashboard/payroll', dashboard_payroll, name="dashboard_payroll"),
    path('load/dashboard/payroll', load_dashboard_payroll,
         name="load_dashboard_payroll"),
    path('load/dashboard/hrd', load_dashboard_hrd, name="load_dashboard_hrd"),
    path('load/dashboard/hrd-current', load_dashboard_hrd_current,
         name="load_dashboard_hrd_current"),
    path('load/chart/payroll', load_chart_payroll, name="load_chart_payroll"),
    path('load/chart/hrd', load_chart_hrd, name="load_chart_hrd"),
    path('load/chart/hrd-current', load_chart_hrd_current,
         name="load_chart_hrd_current"),
    path('load/chart/unit', load_chart_unit, name="load_chart_unit"),


    # run by cron
    path('insert-db-pegawai-per-bulan/', insert_db_pegawai_per_bulan,
         name="insert_db_pegawai_per_bulan"),


    path('tax/', pph_21_formula, name='pph_21')

]
