from django.http import JsonResponse
from dateutil.relativedelta import *
from ui.models import Payroll, PajakStaff
from django.db.models import Sum, FloatField
from django.db.models.functions import Cast
# from ui.utils import rupiah_format

import datetime


def pph_21_formula(request):
    response = JsonResponse({})
    return response


def get_gaji_pokok_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('gaji_pokok', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_tunjangan_lain(lembur, rapel, uang_makan, tunjangan_jabatan,
                       seniority, transport, tunjangan_keluarga,
                       tunjangan_shift, tunjangan_kehadiran,
                       tunjangan_lainnya, potongan_absen, potongan_lainnya,
                       recap=False, nopeg=None, bulan=None):

    res_tunjangan_lain = to_float(lembur) \
        + to_float(rapel) \
        + to_float(uang_makan) \
        + to_float(tunjangan_jabatan) \
        + to_float(seniority) \
        + to_float(transport) \
        + to_float(tunjangan_keluarga) \
        + to_float(tunjangan_shift) \
        + to_float(tunjangan_kehadiran) \
        + to_float(tunjangan_lainnya) \
        - to_float(potongan_absen) \
        - to_float(potongan_lainnya)

    if not recap:
        return res_tunjangan_lain

    else:
        list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

        adders = ['lembur', 'rapel', 'uang_makan',
                  'tunjangan_jabatan', 'seniority', 'transport',
                  'tunjangan_keluarga', 'tunjangan_shift', 'tunjangan_kehadiran',
                  'tunjangan_lainnya']

        substracts = ['potongan_absen', 'potongan_lainnya']

        for entity in adders:
            value = Payroll.objects\
                .filter(staff__nopeg=nopeg)\
                .filter(bulan__in=bulan)\
                .filter(tipe__in=list_tipe)\
                .aggregate(total=Sum(Cast(entity, output_field=FloatField())))

            value = value['total'] if value['total'] != None else 0
            res_tunjangan_lain += value

        for entity in substracts:
            value = Payroll.objects\
                .filter(staff__nopeg=nopeg)\
                .filter(bulan__in=bulan)\
                .filter(tipe__in=list_tipe)\
                .aggregate(total=Sum(Cast(entity, output_field=FloatField())))

            value = value['total'] if value['total'] != None else 0
            res_tunjangan_lain -= value

        return res_tunjangan_lain


def get_asuransi(bpjs_kesehatan_4, asuransi_jkk_jkm,
                 recap=False, nopeg=None, bulan=None):

    res_asuransi = to_float(bpjs_kesehatan_4) \
        + to_float(asuransi_jkk_jkm)

    if not recap:
        return res_asuransi
    else:
        list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

        adders = ['bpjs_kesehatan_4', 'asuransi_jkk_jkm']

        for entity in adders:
            value = Payroll.objects\
                .filter(staff__nopeg=nopeg)\
                .filter(bulan__in=bulan)\
                .filter(tipe__in=list_tipe)\
                .aggregate(total=Sum(Cast(entity, output_field=FloatField())))

            value = value['total'] if value['total'] != None else 0
            res_asuransi += value

        return res_asuransi


def get_penghasilan_bruto_reg(gaji_pokok, tunjangan_lain, asuransi):
    return to_float(gaji_pokok) \
        + tunjangan_lain \
        + asuransi


def get_thr_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('thr', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_bonus_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('bonus', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_penghasilan_bruto_irreg(thr, bonus):
    return to_float(thr) \
        + to_float(bonus)


def get_biaya_jabatan_reg(penghasilan_bruto_reg, durasi_pph_21, recap=False):
    if not recap:
        THRESH = 500000
    else:
        THRESH = 500000 * durasi_pph_21

    BIAYA = penghasilan_bruto_reg * 0.05
    if BIAYA <= THRESH:
        return BIAYA
    else:
        return THRESH


def get_biaya_jabatan_irreg(biaya_jabatan_reg, penghasilan_bruto_irreg, durasi_pph_21,  recap=False):
    if not recap:
        THRESH = 500000
    else:
        THRESH = 500000 * durasi_pph_21

    BIAYA = penghasilan_bruto_irreg * 0.05
    return min(THRESH - biaya_jabatan_reg, BIAYA)


def get_iuran_pensiun_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('astek_jp', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_penghasilan_netto_perbulan_reg(astek_jp, penghasilan_bruto_reg, biaya_jabatan_reg):
    return penghasilan_bruto_reg \
        - biaya_jabatan_reg \
        - to_float(astek_jp)


def get_penghasilan_netto_bulan_sebelum_x(nopeg, bulan):
    try:

        list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

        penghasilan_netto_bulan_sebelum_x = Payroll.objects\
            .filter(staff__nopeg=nopeg)\
            .filter(bulan__in=bulan)\
            .filter(tipe__in=list_tipe)\
            .aggregate(total=Sum(Cast('tax_penghasilan_netto_perbulan_reg', output_field=FloatField())))

        penghasilan_netto_bulan_sebelum_x = penghasilan_netto_bulan_sebelum_x[
            'total'] if penghasilan_netto_bulan_sebelum_x['total'] != None else 0

    except Exception as e:

        penghasilan_netto_bulan_sebelum_x = 0

    return penghasilan_netto_bulan_sebelum_x


def get_penghasilan_netto_pertahun_reg(bulan,
                                       penghasilan_netto_perbulan_reg,
                                       penghasilan_netto_bulan_sebelum_x,
                                       recap=False):

    if not recap:
        return (penghasilan_netto_perbulan_reg * (13-bulan)) \
            + penghasilan_netto_bulan_sebelum_x
    else:
        return penghasilan_netto_perbulan_reg


def get_gaji_netto_sebelum_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('gaji_netto_sebelum', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_pph_21_sebelum_pertahun(nopeg=None, bulan=None):
    list_tipe = ['Gaji Bulanan', 'THR', 'Bonus']

    value = Payroll.objects\
        .filter(staff__nopeg=nopeg)\
        .filter(bulan__in=bulan)\
        .filter(tipe__in=list_tipe)\
        .aggregate(total=Sum(Cast('pph_21_sebelum', output_field=FloatField())))

    value = value['total'] if value['total'] != None else 0

    return value


def get_penghasilan_netto_pertahun_irreg(penghasilan_bruto_irreg, biaya_jabatan_irreg):
    return penghasilan_bruto_irreg \
        - biaya_jabatan_irreg


def get_ptkp(status_pajak):
    if status_pajak == 'TK/0':
        return 54000000
    elif status_pajak == 'TK/1':
        return 58500000
    elif status_pajak == 'TK/2':
        return 63000000
    elif status_pajak == 'TK/3':
        return 67500000
    elif status_pajak == 'K/0':
        return 58500000
    elif status_pajak == 'K/1':
        return 63000000
    elif status_pajak == 'K/2':
        return 67500000
    elif status_pajak == 'K/3':
        return 72000000


def get_pkp_reg(penghasilan_netto_pertahun_reg, gaji_netto_sebelum, ptkp):
    return max(penghasilan_netto_pertahun_reg + gaji_netto_sebelum - ptkp, 0)


def get_pkp_reg_irreg(pkp_reg, penghasilan_netto_pertahun_irreg, ptkp, penghasilan_netto_pertahun_reg, gaji_netto_sebelum):
    return max((pkp_reg
                + (penghasilan_netto_pertahun_irreg - max(ptkp -
                                                          (penghasilan_netto_pertahun_reg + gaji_netto_sebelum), 0))), 0)


def get_pph_21_pertahun_reg_irreg(npwp, pkp_reg_irreg, basis_gaji):
    gross_up = (basis_gaji == 'Gross-Up')

    if pkp_reg_irreg <= 50000000:
        pph_21_pertahun_reg_irreg = pkp_reg_irreg * 0.05
        if gross_up:
            pph_21_pertahun_reg_irreg /= 0.95

    elif pkp_reg_irreg <= 250000000:
        pph_21_pertahun_reg_irreg = 2500000 + \
            ((pkp_reg_irreg - 50000000) * 0.15)
        if gross_up:
            pph_21_pertahun_reg_irreg /= 0.85

    elif pkp_reg_irreg <= 500000000:
        pph_21_pertahun_reg_irreg = 32500000 + \
            ((pkp_reg_irreg - 250000000) * 0.25)
        if gross_up:
            pph_21_pertahun_reg_irreg /= 0.75

    elif pkp_reg_irreg > 500000000:
        pph_21_pertahun_reg_irreg = 95000000 + \
            ((pkp_reg_irreg - 500000000) * 0.3)
        if gross_up:
            pph_21_pertahun_reg_irreg /= 0.7

    if not npwp:
        pph_21_pertahun_reg_irreg *= 1.2

    return pph_21_pertahun_reg_irreg


def get_pph_21_pertahun_reg(npwp, pkp_reg, basis_gaji):
    gross_up = (basis_gaji == 'Gross-Up')

    if pkp_reg <= 50000000:
        pph_21_pertahun_reg = pkp_reg * 0.05
        if gross_up:
            pph_21_pertahun_reg /= 0.95

    elif pkp_reg <= 250000000:
        pph_21_pertahun_reg = 2500000 + ((pkp_reg - 50000000) * 0.15)
        if gross_up:
            pph_21_pertahun_reg /= 0.85

    elif pkp_reg <= 500000000:
        pph_21_pertahun_reg = 32500000 + ((pkp_reg - 250000000) * 0.25)
        if gross_up:
            pph_21_pertahun_reg /= 0.75

    elif pkp_reg > 500000000:
        pph_21_pertahun_reg = 95000000 + ((pkp_reg - 500000000) * 0.3)
        if gross_up:
            pph_21_pertahun_reg /= 0.7

    if not npwp:
        pph_21_pertahun_reg *= 1.2

    return pph_21_pertahun_reg


def get_pph_21_pertahun_irreg(pph_21_pertahun_reg_irreg, pph_21_pertahun_reg):
    return pph_21_pertahun_reg_irreg
    - pph_21_pertahun_reg


def get_pph_21_pertahun_all(pph_21_pertahun_reg_irreg, pph_21_sebelum):
    return pph_21_pertahun_reg_irreg
    - pph_21_sebelum


def get_pph_21_perbulan_all(pph_21_pertahun_reg, pph_21_pertahun_irreg, pph_21_sebelum, recap=False):
    if not recap:
        return (pph_21_pertahun_reg / 12)
        + pph_21_pertahun_irreg
        - pph_21_sebelum
    return 0


def get_pph_21_sistem_fixed_sebelum_x(nopeg, bulan):
    try:
        pph_21_sistem_fixed_sebelum_x = Payroll.objects \
            .filter(staff__nopeg=nopeg) \
            .filter(bulan__in=bulan) \
            .filter(tipe__in=['Gaji Bulanan']) \
            .aggregate(
                total=Sum(Cast('tax_pph_21_perbulan_fixed',
                               output_field=FloatField()))
            )

        pph_21_sistem_fixed_sebelum_x = pph_21_sistem_fixed_sebelum_x[
            'total'] if pph_21_sistem_fixed_sebelum_x['total'] != None else 0

    except:
        pph_21_sistem_fixed_sebelum_x = 0

    return pph_21_sistem_fixed_sebelum_x


def get_adjustment_pph_21(pph_21_pertahun_all, pph_21_sistem_fixed_sebelum_x, recap=False):
    if not recap:
        return 0
    return pph_21_pertahun_all - pph_21_sistem_fixed_sebelum_x


def get_pph_21_perbulan_sistem(pph_21_perbulan_all, adjustment_pph_21):
    return pph_21_perbulan_all + adjustment_pph_21


def to_float(data):
    return float(data) if data != None else 0


def get_month_range(x, y):
    now = datetime.datetime.now().year

    try:
        tahun_x = x.year
        if tahun_x == now:
            bulan_x = x.month
        else:
            bulan_x = 1
    except:
        bulan_x = 1

    try:
        tahun_y = y.year
        if tahun_y == now:
            bulan_y = y.month
        else:
            bulan_y = 12
    except:
        bulan_y = 12

    return get_month_delta(bulan_x, bulan_y), get_duration(bulan_x, bulan_y)


def get_duration(x, y):
    return (y-x) + 1


def get_month_delta(x, y):
    months = list()
    bulan = y-x

    year = datetime.datetime.now().year
    tanggal = datetime.date(year, y, 1)

    while bulan > 0:
        month = tanggal - relativedelta(months=+bulan)
        months.append(month.strftime("%B %Y"))
        bulan -= 1

    return months


def get_month_year(tanggal):
    months = list()
    bulan = tanggal.month

    while bulan > 1:
        bulan -= 1
        month = tanggal - relativedelta(months=+bulan)
        months.append(month.strftime("%B %Y"))

    return months
