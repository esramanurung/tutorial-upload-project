from django.db import models
from django.contrib.auth.models import AbstractUser
from simple_history.models import HistoricalRecords


class Staff(AbstractUser):
    nopeg = models.CharField(max_length=255)
    nama_pegawai = models.CharField(max_length=255, blank=True, null=True)
    npwp = models.CharField(max_length=255, blank=True, null=True)
    nik = models.CharField(max_length=255, blank=True, null=True)
    basis_gaji = models.CharField(
        max_length=255, blank=True, null=True)
    pph_kembali_bulan_terakhir = models.CharField(
        max_length=255, blank=True, null=True)
    jenis_kelamin = models.CharField(max_length=255, blank=True, null=True)
    status_pajak = models.CharField(max_length=255, blank=True, null=True)
    status_kepegawaian = models.CharField(
        max_length=255, blank=True, null=True)
    dept_code = models.CharField(max_length=255, blank=True, null=True)
    gl_account = models.CharField(max_length=255, blank=True, null=True)
    group_expenses = models.CharField(max_length=255, blank=True, null=True)
    dept = models.CharField(max_length=255, blank=True, null=True)
    golongan = models.CharField(max_length=255, blank=True, null=True)
    jabatan = models.CharField(max_length=255, blank=True, null=True)
    tanggal_mulai_kerja = models.DateField(blank=True, null=True)
    mulai_payslip = models.DateField(blank=True, null=True)
    berakhir_payslip = models.DateField(blank=True, null=True)
    nomor_kontrak = models.CharField(max_length=255, blank=True, null=True)
    masa_kontrak = models.CharField(max_length=255, blank=True, null=True)
    ditempatkan_di_perusahaan = models.CharField(
        max_length=255, blank=True, null=True)
    under_supervisi = models.CharField(max_length=255, blank=True, null=True)
    lokasi = models.CharField(max_length=255, blank=True, null=True)
    nomor_id_card = models.CharField(max_length=255, blank=True, null=True)
    masa_berlaku_id_card = models.DateField(blank=True, null=True)
    tempat_lahir = models.CharField(max_length=255, blank=True, null=True)
    tanggal_lahir = models.DateField(blank=True, null=True)
    alamat_domisili_sekarang = models.CharField(
        max_length=255, blank=True, null=True)
    alamat_email_perusahaan = models.EmailField(blank=True, null=True)
    alamat_email_pribadi = models.EmailField(blank=True, null=True)
    nomor_telpon = models.CharField(max_length=255, blank=True, null=True)
    nomor_kartu_keluarga = models.CharField(
        max_length=255, blank=True, null=True)
    nama_suami_istri = models.CharField(max_length=255, blank=True, null=True)
    nama_anak_satu = models.CharField(max_length=255, blank=True, null=True)
    nama_anak_dua = models.CharField(max_length=255, blank=True, null=True)
    nama_anak_tiga = models.CharField(max_length=255, blank=True, null=True)
    nomor_bpjs_kesehatan = models.CharField(
        max_length=255, blank=True, null=True)
    masa_berlaku_bpjs_kesehatan = models.DateField(blank=True, null=True)
    nomor_bpjs_tenagakerja = models.CharField(
        max_length=255, blank=True, null=True)
    masa_berlaku_bpjs_tenagakerja = models.DateField(blank=True, null=True)
    premi_asuransi = models.CharField(max_length=255, blank=True, null=True)
    pendidikan_terakhir = models.CharField(
        max_length=255, blank=True, null=True)
    sertifikat_satu = models.CharField(max_length=255, blank=True, null=True)
    sertifikat_dua = models.CharField(max_length=255, blank=True, null=True)
    sertifikat_tiga = models.CharField(max_length=255, blank=True, null=True)
    sertifikat_empat = models.CharField(max_length=255, blank=True, null=True)
    training_diikuti = models.CharField(max_length=255, blank=True, null=True)
    training_satu = models.CharField(max_length=255, blank=True, null=True)
    training_dua = models.CharField(max_length=255, blank=True, null=True)
    training_tiga = models.CharField(max_length=255, blank=True, null=True)
    training_empat = models.CharField(max_length=255, blank=True, null=True)
    referensi = models.CharField(max_length=255, blank=True, null=True)
    access = models.CharField(max_length=10, blank=True, null=True)
    profile_picture = models.ImageField(
        upload_to="profile-pict/", blank=True, null=True)

    def __str__(self):
        return f'{self.nama_pegawai}({self.nopeg})'


class Payroll(models.Model):
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    bulan = models.CharField(max_length=255, blank=True, null=True)
    tipe = models.CharField(
        max_length=255, default='Gaji Bulanan', blank=True, null=True)
    gaji_pokok = models.CharField(max_length=255, blank=True, null=True)
    potongan_absen = models.CharField(max_length=255, blank=True, null=True)
    lembur = models.CharField(max_length=255, blank=True, null=True)
    thr = models.CharField(max_length=255, blank=True, null=True)
    rapel = models.CharField(max_length=255, blank=True, null=True)
    bonus = models.CharField(max_length=255, blank=True, null=True)
    uang_makan = models.CharField(max_length=255, blank=True, null=True)
    tunjangan_jabatan = models.CharField(max_length=255, blank=True, null=True)
    seniority = models.CharField(max_length=255, blank=True, null=True)
    transport = models.CharField(max_length=255, blank=True, null=True)
    tunjangan_keluarga = models.CharField(
        max_length=255, blank=True, null=True)
    tunjangan_shift = models.CharField(
        max_length=255, blank=True, null=True)
    tunjangan_kehadiran = models.CharField(
        max_length=255, blank=True, null=True)
    tunjangan_lainnya = models.CharField(
        max_length=255, blank=True, null=True)
    tunjangan_pph_21 = models.CharField(max_length=255, blank=True, null=True)
    gaji_bruto = models.CharField(max_length=255, blank=True, null=True)
    salary_exclude_ot = models.CharField(max_length=255, blank=True, null=True)
    pph_21 = models.CharField(max_length=255, blank=True, null=True)
    astek_jp = models.CharField(max_length=255, blank=True, null=True)
    bpjs_kesehatan_1 = models.CharField(max_length=255, blank=True, null=True)
    bpjs_kesehatan_4 = models.CharField(max_length=255, blank=True, null=True)
    asuransi_jkk_jkm = models.CharField(max_length=255, blank=True, null=True)
    iuran_spsi = models.CharField(max_length=255, blank=True, null=True)
    potongan_lainnya = models.CharField(max_length=255, blank=True, null=True)
    total_potongan = models.CharField(max_length=255, blank=True, null=True)
    gaji_netto = models.CharField(max_length=255, blank=True, null=True)
    nomor_rekening = models.CharField(max_length=255, blank=True, null=True)
    bank = models.CharField(max_length=255, blank=True, null=True)

    gaji_netto_sebelum = models.CharField(
        max_length=255, blank=True, null=True)
    pph_21_sebelum = models.CharField(max_length=255, blank=True, null=True)

    # tax
    tax_gaji_pokok = models.CharField(
        max_length=255, blank=True, null=True)
    tax_thr = models.CharField(
        max_length=255, blank=True, null=True)
    tax_bonus = models.CharField(
        max_length=255, blank=True, null=True)
    tax_iuran_pensiun = models.CharField(
        max_length=255, blank=True, null=True)
    tax_gaji_netto_sebelum = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_sebelum = models.CharField(
        max_length=255, blank=True, null=True)
    tax_tunjangan_lain = models.CharField(
        max_length=255, blank=True, null=True)
    tax_asuransi = models.CharField(max_length=255, blank=True, null=True)
    tax_penghasilan_bruto_reg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_penghasilan_bruto_irreg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_biaya_jabatan_reg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_biaya_jabatan_irreg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_penghasilan_netto_perbulan_reg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_penghasilan_netto_pertahun_reg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_penghasilan_netto_pertahun_irreg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_ptkp = models.CharField(max_length=255, blank=True, null=True)
    tax_pkp_reg = models.CharField(max_length=255, blank=True, null=True)
    tax_pkp_reg_irreg = models.CharField(max_length=255, blank=True, null=True)
    tax_pph_21_pertahun_reg_irreg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_pertahun_reg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_pertahun_irreg = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_pertahun_all = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_perbulan_all = models.CharField(
        max_length=255, blank=True, null=True)
    tax_adjustment_pph_21 = models.CharField(
        max_length=255, blank=True, null=True)
    tax_total_pph_21_fixed_sebelum = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_perbulan_sistem = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_perbulan_manual = models.CharField(
        max_length=255, blank=True, null=True)
    tax_pph_21_perbulan_fixed = models.CharField(
        max_length=255, blank=True, null=True)
    penghitung_pph_21 = models.CharField(
        default='sistem', max_length=255, blank=True, null=True)

    status = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        unique_together = (("staff", "bulan", "tipe"),)

    def __str__(self):
        return f'{self.staff.nama_pegawai} ({self.staff.nopeg}) {self.bulan} {self.tipe}'


class History(models.Model):
    user = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = "histories"

# https://django-simple-history.readthedocs.io/en/latest/quick_start.html


class StatusStaff(models.Model):
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    status_pegawai = models.CharField(max_length=255, blank=True, null=True)
    status_aktif = models.CharField(max_length=255, blank=True, null=True)
    keterangan_aktif = models.CharField(max_length=255, blank=True, null=True)
    status_tidak_aktif = models.CharField(
        max_length=255, blank=True, null=True)
    finish_contract = models.CharField(max_length=255, blank=True, null=True)
    pengganti = models.CharField(max_length=255, blank=True, null=True)
    keterangan_tidak_aktif = models.CharField(
        max_length=255, blank=True, null=True)

    status_history = HistoricalRecords()


class PajakStaff(models.Model):
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    gaji_pokok = models.CharField(max_length=255, blank=True, null=True)
    tunjangan_pajak = models.CharField(max_length=255, blank=True, null=True)
    tunjangan_lain = models.CharField(max_length=255, blank=True, null=True)
    asuransi = models.CharField(max_length=255, blank=True, null=True)
    penghasilan_bruto = models.CharField(max_length=255, blank=True, null=True)
    biaya_jabatan = models.CharField(max_length=255, blank=True, null=True)
    iuran_pensiun = models.CharField(max_length=255, blank=True, null=True)
    penghasilan_netto = models.CharField(max_length=255, blank=True, null=True)
    penghasilan_pertahun_sebelum_thr = models.CharField(
        max_length=255, blank=True, null=True)
    thr = models.CharField(max_length=255, blank=True, null=True)
    penghasilan_pertahun = models.CharField(
        max_length=255, blank=True, null=True)
    ptkp = models.CharField(max_length=255, blank=True, null=True)
    pkp = models.CharField(max_length=255, blank=True, null=True)
    pph_21_pertahun = models.CharField(max_length=255, blank=True, null=True)
    pph_21_perbulan = models.CharField(max_length=255, blank=True, null=True)


class Dashboard(models.Model):
    jumlah_pegawai_aktif = models.IntegerField(blank=True, null=True)
    jumlah_pegawai_tidak_aktif = models.IntegerField(blank=True, null=True)
    unit = models.CharField(max_length=255, blank=True, null=True)
    date = models.CharField(max_length=255, blank=True, null=True)
