"""titan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from ui.views import mobile_homepage, login, auth_login, auth_logout, edit_user, change_pass, upload_profile_pict, webmail_redirect
from compro import urls as compro


admin.autodiscover()

urlpatterns = [
    path('admin/debug/', admin.site.urls),
    path('admin/', include('ui.urls')),
    url(r'^$', RedirectView.as_view(
        url='admin/dashboard/payroll', permanent=True), name='$'),
    path('pegawai/', mobile_homepage, name='mobile'),
    path('accounts/login/', login, name='login'),
    path('auth/login/', auth_login, name='auth_login'),
    path('auth/logout/', auth_logout, name='auth_logout'),
    path('edit/<nopeg>/', edit_user, name='edit_user'),
    path('update/picture/', upload_profile_pict, name='upload_profile_pict'),
    path('change/password/<nopeg>/', change_pass, name='change_pass'),
    path('webmail', webmail_redirect, name='webmail_redirect')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
