from django.db import connection

def get_units():
    cursor = connection.cursor()
    cursor.execute('''select distinct substr(departemen_sekarang,1,2) from ui_staff''')
    unitsFetch = cursor.fetchall()
    cursor.close()
    return unitsFetch

def count_pegawai_per_unit(keterangan, unit):
    cursor = connection.cursor()
    cursor.execute('''select count(*)
                        from ui_staff as staff, ui_statusstaff as status
                        where status.status_pegawai = %s and staff.id = status.staff_id and 
                        substr(staff.departemen_sekarang,1,2) = %s''', [keterangan, unit])
    countPegawaiFetch = cursor.fetchone()
    cursor.close()
    return countPegawaiFetch[0]

def count_pegawai(keterangan):
    cursor = connection.cursor()
    cursor.execute('''select count(*)
                        from ui_staff as staff, ui_statusstaff as status
                        where status.status_pegawai = %s and staff.id = status.staff_id''', [keterangan])
    countPegawaiFetch = cursor.fetchone()
    cursor.close()
    return countPegawaiFetch[0]