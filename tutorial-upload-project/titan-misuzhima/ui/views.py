from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required,  permission_required
from django.contrib.auth import login as user_login, logout as user_logout, authenticate
from django.http import JsonResponse
from django.db.models import Q
from django.views.generic import View

from ui.utils import *
from ui.models import Staff, Payroll, StatusStaff, Dashboard
from ui.query import *
from ui.render import Render
from ui.bindings import *

import datetime
import dateutil.relativedelta
import re
import xlwt
import xlsxwriter
from PIL import Image
import io
from io import BytesIO
import operator
import os
import json
response = {}


@login_required(login_url='/accounts/login/')
def mobile_homepage(request):
    months = get_months()
    staff = request.user
    recap = list()

    for month in months:
        pdfs = list()
        for tipe in ['Gaji Bulanan', 'THR', 'Bonus']:
            try:
                payroll = Payroll.objects.get(
                    staff=staff, bulan=month, tipe=tipe)
                if payroll.status == 'Published':
                    pdfs.append((tipe, month, True))
                else:
                    pdfs.append((tipe, month, False))
            except:
                pdfs.append((tipe, month, False))

        recap.append(pdfs)

    response['staff'] = staff
    response['pdfs'] = recap

    return render(request, 'content/mobile/index.html', response)


@login_required
@permission_required(['ui.view_staff', 'ui.view_payroll'], raise_exception=True)
def homepage(request):
    return render(request, 'content/homepage.html')


@login_required
@permission_required('ui.view_staff', raise_exception=True)
def staff_index(request):
    # get departement list
    dept = Staff.objects.values('dept').distinct()

    response['dept'] = dept

    # get departement param
    query = request.GET.get('filter', '')

    search = None

    if query:
        search = Staff.objects.filter(
            Q(dept__iexact=query)).order_by('pk')[::-1]

    if not search:
        search = Staff.objects.filter().order_by(
            'pk').exclude(access__in=['finance', 'hrd', 'director'])[::-1]

    response['query'] = query
    response['staffs'] = search

    return render(request, 'content/staff/index.html', response)


@login_required
@permission_required('ui.view_payroll', raise_exception=True)
def payroll_index(request):
    months = get_months()
    response['months'] = get_months

    q = request.GET.get('bulan', '')
    f = request.GET.get('tipe', '')

    now = datetime.datetime.now()

    if len(q) == 0 and len(f) == 0:
        q = months[(12-now.month)]
        f = 'Gaji Bulanan'
    elif len(q) > 0 and len(f) == 0:
        f = 'Gaji Bulanan'
    elif len(q) == 0 and len(f) > 0:
        q = months[(12-now.month)]

    search = Payroll.objects.filter(Q(bulan__iexact=q, tipe__iexact=f))[::-1]

    response['month'] = q
    response['types'] = f
    response['m_val'] = convert_to_datetime(q)

    response['payrolls'] = search

    grand_total = 0

    for gaji in search:
        try:
            grand_total += int(float(gaji.gaji_netto))
        except:
            continue

    if len(search) > 0:
        is_published = (search[0].status == "Published")
    else:
        is_published = False

    response['grand_total'] = grand_total
    response['is_published'] = is_published

    return render(request, 'content/payroll/index.html', response)


def payroll_history(request):
    history = History.objects.all().order_by('-date')

    response['histories'] = history

    return render(request, 'content/payroll/history.html', response)


def staff_detail(request, nopeg):
    try:
        staff = Staff.objects.get(nopeg=nopeg)
        statusStaff = StatusStaff.objects.get(staff__nopeg=nopeg)
        response['staff'] = staff
        response['statusStaff'] = statusStaff
    except ObjectDoesNotExist:
        pass

    return render(request, 'content/staff/detail.html', response)


def staff_add(request):
    response['add'] = True
    return render(request, 'content/staff/add_form.html', response)


def payroll_add(request, bulan, tipe):
    response['add'] = True
    response['months'] = get_months
    response['bulan'] = bulan
    response['tipe'] = tipe
    return render(request, 'content/payroll/add_form.html', response)


def payroll_view(request, filetype, nopeg, bulan, tipe):
    try:
        staff = Staff.objects.get(nopeg=nopeg)
        payroll = Payroll.objects.get(staff=staff, bulan=bulan, tipe=tipe)
        response['payroll'] = payroll
        response['type'] = filetype
        response['tipe'] = tipe
    except ObjectDoesNotExist:
        pass

    return render(request, 'content/payroll/view.html', response)


def staff_edit(request, nopeg):
    try:
        staff = Staff.objects.get(nopeg=nopeg)
        response['staff'] = staff
    except ObjectDoesNotExist:
        pass

    return render(request, 'content/staff/edit_form.html', response)


def edit_status_form(request, nopeg):
    try:
        statusStaff = StatusStaff.objects.get(staff__nopeg=nopeg)
        response['statusStaff'] = statusStaff
    except ObjectDoesNotExist:
        pass

    return render(request, 'content/staff/edit_status_form.html', response)


def payroll_edit(request, nopeg, bulan, tipe):
    response['months'] = get_months
    try:
        staff = Staff.objects.get(nopeg=nopeg)
        payroll = Payroll.objects.get(staff=staff, bulan=bulan, tipe=tipe)
        response['payroll'] = payroll
        response['tipe'] = tipe
    except ObjectDoesNotExist:
        pass

    return render(request, 'content/payroll/edit_form.html', response)


@permission_required('ui.view_staff', raise_exception=True)
def staff_upload(request):
    return render(request, 'content/staff/upload.html')


@permission_required('ui.view_payroll', raise_exception=True)
def payroll_upload(request):
    published = Payroll.objects.filter(
        status='Published').values_list('bulan').distinct()
    months = get_months()

    for bulan in published:
        if bulan[0] in months:
            months.remove(bulan[0])

    response['months'] = months
    return render(request, 'content/payroll/upload.html', response)


@permission_required('ui.add_payroll', raise_exception=True)
def profile_index(request):
    return render(request, 'content/profile/index.html', response)


@permission_required('ui.add_payroll', raise_exception=True)
def profile_access(request):
    profiles = Staff.objects.filter(
        Q(access__iexact='hrd') | Q(access__iexact='finance'))
    response['profiles'] = profiles
    return render(request, 'content/profile/add.html', response)


def export_index(request):
    response['months'] = Payroll.objects\
        .filter(tipe='Gaji Bulanan')\
        .values('bulan').distinct()
    return render(request, 'content/export/index.html', response)


# CRUD STAFF
@csrf_exempt
def add_staff_access(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        role = request.POST['role']

        user = Staff(
            username=username,
            password=password,
            nopeg=username,
            nama_pegawai=username,
            alamat_email_perusahaan=email,
            access=role
        )

        user.save()

        if role == 'hrd':
            user.user_permissions.add(24)
        else:
            user.user_permissions.add(44)

        return redirect('/admin/profile/access/')
    else:
        return redirect('/admin/profile/access/')


@csrf_exempt
def edit_staff_access(request, pk):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        role = request.POST['role']

        user = Staff.objects.get(pk=pk)

        user.username = username
        user.alamat_email_perusahaan = email
        user.nopeg = username
        user.access = role
        user.nama_pegawai = username
        user.save()

        if role == "hrd":
            user.user_permissions.remove(24)
            user.user_permissions.remove(32)
            user.user_permissions.add(24)
        else:
            user.user_permissions.remove(24)
            user.user_permissions.remove(32)
            user.user_permissions.add(32)
        return redirect('/admin/profile/access/')
    else:
        return redirect('/admin/profile/access/')


def edit_staff_index(request, pk):
    user = Staff.objects.get(pk=pk)
    response = {'staff': user}
    return render(request, 'content/profile/edit.html', response)


def delete_staff_access(request, pk):
    user = Staff.objects.get(pk=pk)
    user.delete()
    return redirect('/admin/profile/access/')


@csrf_exempt
def publish_payroll(request, bulan, tipe):
    payroll = Payroll.objects.filter(Q(bulan__iexact=bulan, tipe__iexact=tipe))
    payroll.update(status="Published")

    history = History(
        status="[PUBLISH] Gaji Bulan %s" % (bulan)
    )

    history.save()

    return redirect(f'/admin/payroll?bulan={bulan}&tipe={tipe}')


@csrf_exempt
def recalculate_tax(request, bulan, tipe):
    rows = Payroll.objects.filter(bulan=bulan, tipe=tipe).values_list(
        'staff__nopeg', 'bulan', 'gaji_pokok', 'potongan_absen', 'lembur', 'thr', 'rapel',
        'uang_makan', 'bonus', 'tunjangan_jabatan', 'seniority',
        'transport', 'tunjangan_keluarga', 'tunjangan_shift', 'tunjangan_kehadiran',
        'tunjangan_lainnya', 'tunjangan_pph_21', 'gaji_bruto', 'salary_exclude_ot',
        'pph_21', 'astek_jp', 'bpjs_kesehatan_1', 'bpjs_kesehatan_4',
        'asuransi_jkk_jkm', 'iuran_spsi', 'potongan_lainnya', 'total_potongan', 'gaji_netto',
        'nomor_rekening', 'bank',
    )

    for row in rows:
        datas = list(row)
        update_obj_payroll(datas, bulan, tipe, method='manual')

    return redirect(f'/admin/payroll?bulan={bulan}&tipe={tipe}')


@csrf_exempt
def add_staff_detail(request):
    forms = request.POST
    datas = list()
    for v in forms.values():
        datas.append(v)

    save_obj_staff(datas, method='manual')

    return redirect('/admin/staff/')


@csrf_exempt
def add_payroll_detail(request):
    forms = request.POST
    datas = list()
    for v in forms.values():
        v = re.sub(r'[^\w. ]', ' ', v)
        datas.append(v)

    _nopeg = datas[0]
    _month = datas[1]
    _tipe = datas.pop(2)

    if check_obj_payroll(_nopeg, _month, _tipe):
        update_obj_payroll(datas, _month, _tipe, method='manual')
    else:
        save_obj_payroll(datas, _month, _tipe, method='manual')

    return redirect('/admin/payroll/')


@csrf_exempt
def edit_status_detail(request):
    forms = request.POST
    datas = list()
    for v in forms.values():
        datas.append(v)

    status_staff_form(datas, action='edit')

    return redirect(f'/admin/staff/detail/{datas[0]}/')


@csrf_exempt
def edit_staff_detail(request):
    forms = request.POST
    datas = list()
    for v in forms.values():
        datas.append(v)

    update_obj_staff(datas, method='manual')

    return redirect(f'/admin/staff/detail/{datas[1]}/')


@csrf_exempt
def edit_payroll_detail(request, pk):
    forms = request.POST
    datas = list()
    for v in forms.values():
        v = re.sub(r'[^\w. ]', '', v)
        datas.append(v)

    payroll = Payroll.objects.get(pk=pk)
    _nopeg = payroll.staff.nopeg
    _month = payroll.bulan
    _tipe = payroll.tipe

    datas.insert(0, _nopeg)
    datas.insert(1, _month)

    tax_manual = datas.pop()
    tax_calculate_by = datas.pop()

    update_obj_payroll(datas, _month, _tipe,
                       method='manual',
                       tax_manual=tax_manual,
                       penghitung=tax_calculate_by)

    return redirect('/admin/payroll/')


@csrf_exempt
def delete_staff(request, nopeg):
    try:
        staff = Staff.objects.get(nopeg=nopeg)
        staff.kondisi = "Deactive"
        staff.save()
    except Exception:
        pass

    return redirect('/admin/staff/')


@csrf_exempt
def upload_staff_detail(request):
    if request.FILES:
        uploads = request.FILES.getlist('files')
        for _file in uploads:
            response = import_user(_file)
        if len(response.get('staff_csv')) > 0:
            return JsonResponse({
                'status': 'editable',
                'data': response
            })
        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})


@csrf_exempt
def upload_staff_edit(request):
    if request.method == 'POST':
        datas = json.loads(request.body)
        for data in datas:
            update_obj_staff(data)
        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})


@csrf_exempt
def upload_payroll_detail(request):
    if request.FILES:
        types = request.POST.getlist('types')
        months = request.POST.getlist('months')
        uploads = request.FILES.getlist('files')
        for i in range(len(uploads)):
            response = import_monthly_payroll(uploads[i], months[i], types[i])
            if len(response.get('error')) > 0:
                return JsonResponse({'status': 'error', 'data': response})
            # retrieve_payroll(uploads[i], months[i])

        return JsonResponse({'status': 'success'})
    else:
        return JsonResponse({'status': 'error'})


def login(request):
    return render(request, 'content/mobile/login.html', response)


def edit_user(request, nopeg):
    response['nopeg'] = nopeg
    return render(request, 'content/mobile/edit.html', response)


@csrf_exempt
def auth_login(request):
    if request.method == 'POST':
        username = request.POST['username']
        raw_password = request.POST['password']
        try:

            user = authenticate(username=username, password=raw_password)

            if not user:
                user = Staff.objects.get(
                    username=username, password=raw_password)

            user_login(request, user,
                       backend='django.contrib.auth.backends.ModelBackend')

            if user.access in ['finance', 'director']:
                return redirect('/admin/dashboard/payroll')
            if user.access in ['hrd']:
                return redirect('/admin/dashboard/staff')
            return redirect('/pegawai')

        except:
            response['message'] = "Username atau Password Salah"
            return redirect('/pegawai')


@csrf_exempt
def upload_profile_pict(request):
    if request.method == 'POST':
        picture = request.FILES['file']
        user = Staff.objects.get(nopeg=request.user.nopeg)

        user.profile_picture = picture
        user.save()

    return JsonResponse({'status': 'success add'})


@csrf_exempt
def change_pass(request, nopeg):
    if request.method == 'POST':
        old = request.POST['password1']
        new = request.POST['password2']
        exist = Staff.objects.filter(username=nopeg, password=old)
        if exist:
            user = Staff.objects.get(username=nopeg, password=old)
            user.password = new
            user.save()
            user_logout(request)

        return redirect('/')


@csrf_exempt
def auth_logout(request):
    user_logout(request)
    return redirect('/')


def export_access_xlsx(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="staff.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Staff')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ["Username", "Nama Pegawai", "Password"]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Staff.objects.exclude(access__in=['hrd', 'finance', 'director']).values_list(
        "username", "nama_pegawai", "password")

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


def dot_matrix_format(request, bulan, tipe):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    output = io.BytesIO()

    workbook = xlsxwriter.Workbook(
        output, {'in_memory': True, 'remove_timezone': True})
    worksheet = workbook.add_worksheet('Payroll System')

    worksheet.set_landscape()
    worksheet.set_paper(1)
    worksheet.set_margins(0, 0, 0, 0)

    worksheet.set_column('A:A', 10)
    worksheet.set_column('B:B', 5)
    worksheet.set_column('C:C', 26)
    worksheet.set_column('D:D', 14)
    worksheet.set_column('E:E', 2)
    worksheet.set_column('F:F', 12)
    worksheet.set_column('G:G', 15)
    worksheet.set_column('H:H', 16)

    miz_title = workbook.add_format({
        'font_size': 11,
        'font_name': "FXMatrix105MonoElite",
    })

    miz_title_sub = workbook.add_format({
        'font_size': 10,
        'font_name': "FXMatrix105MonoElite",
    })

    merge_format = workbook.add_format({
        'align': 'right',
        'font_size': 14,
        'font_name': "FXMatrix105MonoElite",
    })

    table_head = workbook.add_format({
        'top': 1,
        'bottom': 1,
        'font_size': 11,
        'font_name': "FXMatrix105MonoElite",
    })

    table_head_right = workbook.add_format({
        'top': 1,
        'bottom': 1,
        'font_size': 11,
        'align': 'right',
        'font_name': "FXMatrix105MonoElite",
    })

    border = workbook.add_format({
        'border': 1,
        'align': 'right',
        'font_size': 11,
        'font_name': "FXMatrix105MonoElite",
    })

    italic = workbook.add_format({
        'font_size': 9,
        'italic': 1,
        'font_name': "FXMatrix105MonoElite",
    })

    small = workbook.add_format({
        'underline': 1,
        'font_size': 8,
        'font_name': "FXMatrix105MonoElite",
    })

    big = workbook.add_format({
        'font_size': 12,
        'font_name': "FXMatrix105MonoElite",
    })

    normal = workbook.add_format({
        'font_size': 10,
        'font_name': "FXMatrix105MonoElite",
    })

    normal_right = workbook.add_format({
        'font_size': 10,
        'align': 'right',
        'font_name': "FXMatrix105MonoElite",
    })

    ADDR = 0

    payroll = Payroll.objects.filter(
        bulan=bulan, tipe=tipe, status="Published")

    for p in payroll:
        worksheet.insert_image(f'A{2+ADDR}', os.path.join(
            BASE_DIR, 'compro/static/img/logo_bas.png'), {'x_scale': 0.135, 'y_scale': 0.12})

        worksheet.write(
            f'C{2+ADDR}', 'PT Mizushima Metal Works Indonesia', miz_title)
        worksheet.write(
            f'C{3+ADDR}', 'Jl. Surya Utama Kav I 26 B2', miz_title_sub)
        worksheet.write(
            f'C{4+ADDR}', 'Kutanegara, Ciampel, Karawang 41363', miz_title_sub)

        worksheet.write(f'A{10+ADDR}', 'INFORMASI', table_head)
        worksheet.write(f'B{10+ADDR}', '', table_head)
        worksheet.write(f'C{10+ADDR}', 'PENDAPATAN', table_head)
        worksheet.write(f'D{10+ADDR}', '', table_head)
        worksheet.write(f'E{10+ADDR}', '', table_head)
        worksheet.write(f'F{10+ADDR}', 'PENGURANGAN', table_head)
        worksheet.write(f'G{10+ADDR}', '', table_head)
        worksheet.write(f'H{10+ADDR}', '', table_head)

        worksheet.merge_range(f'F{2+ADDR}:H{2+ADDR}',
                              'PRIVATE & CONFIDENTAL', merge_format)

        mapping = dict()

        mapping[f'A{6+ADDR}'] = 'NIK'
        mapping[f'B{6+ADDR}'] = f':{p.staff.nik}'
        mapping[f'F{6+ADDR}'] = 'BANK'
        mapping[f'G{6+ADDR}'] = f':{p.bank}'

        mapping[f'A{7+ADDR}'] = 'Nama'
        mapping[f'B{7+ADDR}'] = f':{p.staff.nama_pegawai}'
        mapping[f'F{7+ADDR}'] = 'Transfer'
        mapping[f'G{7+ADDR}'] = f':A/C {p.nomor_rekening}'

        mapping[f'A{8+ADDR}'] = 'Jabatan'
        mapping[f'B{8+ADDR}'] = f':{p.staff.jabatan}'
        mapping[f'F{8+ADDR}'] = 'Tanggal'
        mapping[f'G{8+ADDR}'] = f':{datetime.date.today().strftime("%d %B %Y")}'

        mapping[f'A{11+ADDR}'] = 'Year to date'
        mapping[f'C{11+ADDR}'] = 'Gaji Pokok'
        mapping[f'D{11+ADDR}'] = rupiah_format(p.gaji_pokok)
        mapping[f'F{11+ADDR}'] = 'PPH-21 per Bulan'
        mapping[f'H{11+ADDR}'] = rupiah_format(p.pph_21)

        mapping[f'A{12+ADDR}'] = bulan
        mapping[f'C{12+ADDR}'] = 'Potongan Absen'
        mapping[f'D{12+ADDR}'] = f'-{rupiah_format(p.potongan_absen)}'
        mapping[f'F{12+ADDR}'] = 'ASTEK JP 0,003%'
        mapping[f'H{12+ADDR}'] = rupiah_format(p.astek_jp)

        mapping[f'C{13+ADDR}'] = 'Lembur'
        mapping[f'D{13+ADDR}'] = rupiah_format(p.lembur)
        mapping[f'F{13+ADDR}'] = 'BPJS KES. 1%'
        mapping[f'H{13+ADDR}'] = rupiah_format(p.bpjs_kesehatan_1)

        mapping[f'C{14+ADDR}'] = 'THR'
        mapping[f'D{14+ADDR}'] = rupiah_format(p.thr)
        mapping[f'F{14+ADDR}'] = 'BPJS KES. 4%'
        mapping[f'H{14+ADDR}'] = rupiah_format(p.bpjs_kesehatan_4)

        mapping[f'C{15+ADDR}'] = 'Rapel'
        mapping[f'D{15+ADDR}'] = rupiah_format(p.rapel)
        mapping[f'F{15+ADDR}'] = 'Asuransi JKK & JKM (1.19%)'
        mapping[f'H{15+ADDR}'] = rupiah_format(p.asuransi_jkk_jkm)

        mapping[f'C{16+ADDR}'] = 'Bonus'
        mapping[f'D{16+ADDR}'] = rupiah_format(p.bonus)
        mapping[f'F{16+ADDR}'] = 'Potongan Lainnya'
        mapping[f'H{16+ADDR}'] = rupiah_format(p.potongan_lainnya)

        mapping[f'C{17+ADDR}'] = 'Uang Makan'
        mapping[f'D{17+ADDR}'] = rupiah_format(p.uang_makan)
        mapping[f'F{17+ADDR}'] = 'Iuran SPSI'
        mapping[f'H{17+ADDR}'] = rupiah_format(p.iuran_spsi)

        mapping[f'C{18+ADDR}'] = 'Tunj. Jabatan'
        mapping[f'D{18+ADDR}'] = rupiah_format(p.tunjangan_jabatan)

        mapping[f'C{19+ADDR}'] = 'Seniority'
        mapping[f'D{19+ADDR}'] = rupiah_format(p.seniority)

        mapping[f'C{20+ADDR}'] = 'Transport'
        mapping[f'D{20+ADDR}'] = rupiah_format(p.transport)

        mapping[f'C{21+ADDR}'] = 'Tunj. Keluarga'
        mapping[f'D{21+ADDR}'] = rupiah_format(p.tunjangan_keluarga)

        mapping[f'C{22+ADDR}'] = 'Tunj. Shift'
        mapping[f'D{22+ADDR}'] = rupiah_format(p.tunjangan_shift)

        mapping[f'C{23+ADDR}'] = 'Tunj. Kehadiran'
        mapping[f'D{23+ADDR}'] = rupiah_format(p.tunjangan_kehadiran)

        mapping[f'C{24+ADDR}'] = 'BPJS 4%'
        mapping[f'D{24+ADDR}'] = rupiah_format(p.bpjs_kesehatan_4)

        mapping[f'C{25+ADDR}'] = 'Asuransi JKK & JKM (1.19%)'
        mapping[f'D{25+ADDR}'] = rupiah_format(p.asuransi_jkk_jkm)

        mapping[f'C{26+ADDR}'] = 'Tunj. Lainnya'
        mapping[f'D{26+ADDR}'] = rupiah_format(p.tunjangan_lainnya)

        mapping[f'C{27+ADDR}'] = 'Tunj. PPH-21'
        mapping[f'D{27+ADDR}'] = rupiah_format(p.tunjangan_pph_21)

        for k, v in mapping.items():
            if any(key in k for key in ['D', 'H']):
                worksheet.write(k, v, normal_right)
            elif 'date' in v:
                worksheet.write(k, v, small)
            else:
                worksheet.write(k, v, normal)

        worksheet.write(f'A{28+ADDR}', '', table_head)
        worksheet.write(f'B{28+ADDR}', '', table_head)
        worksheet.write(f'C{28+ADDR}', 'GAJI BRUTO', table_head)
        worksheet.write(
            f'D{28+ADDR}', rupiah_format(p.gaji_bruto), table_head_right)
        worksheet.write(f'E{28+ADDR}', '', table_head)
        worksheet.write(f'F{28+ADDR}', 'TOTAL PENGURANGAN', table_head)
        worksheet.write(f'G{28+ADDR}', '', table_head)
        worksheet.write(
            f'H{28+ADDR}', rupiah_format(p.total_potongan), table_head_right)

        worksheet.write(f'F{30+ADDR}', 'Gajih Bersih', big)
        worksheet.write(f'H{30+ADDR}', rupiah_format(p.gaji_netto), border)

        worksheet.write(
            f'A{32+ADDR}', '**Slip Gaji ini dicetak menggunakan HR System, tidak membutuhkan stamp atau tanda tangan**', italic)
        worksheet.write(
            f'A{33+ADDR}', ' ')

        ADDR += 40

    workbook.close()

    output.seek(0)

    response = HttpResponse(output.read(
    ), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = f"attachment; filename=Gaji Dot Matrix {tipe} {bulan} .xls"

    output.close()

    return response


def export_staff_xlsx(request):
    MULTIPLIER = 20
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    output = io.BytesIO()

    workbook = xlsxwriter.Workbook(
        output, {'in_memory': True, 'remove_timezone': True})
    worksheet = workbook.add_worksheet('Data Karyawan')
    worksheet.insert_image('A1', os.path.join(
        BASE_DIR, 'compro/static/img/logo_bas.png'), {'x_scale': 0.2, 'y_scale': 0.2})
    merge_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'right',
        'valign': 'vcenter',
        'font_size': 14,
        'font_color': '#44546A'})

    title_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'fg_color': '#f4b183',
        'valign': 'vcenter',
        'font_size': 12,
        'font_color': 'black'
    })

    title_blue_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'fg_color': '#9DC3E6',
        'valign': 'vcenter',
        'font_size': 12,
        'font_color': 'black'
    })
    counter_format = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'fg_color': 'white',
        'valign': 'vcenter',
        'font_size': 11,
        'font_color': 'black'
    })
    content_format = workbook.add_format({
        'bold': 0,
        'border': 1,
        'align': 'center',
        'fg_color': 'white',
        'valign': 'vcenter',
        'font_size': 11,
        'font_color': 'black'
    })

    date_format = workbook.add_format({
        'bold': 0,
        'border': 1,
        'align': 'center',
        'fg_color': 'white',
        'valign': 'vcenter',
        'font_size': 11,
        'font_color': 'black',
        'num_format': 'd-mmm-yyyy'
    })

    worksheet.merge_range(
        'D1:I1', 'PT. MIZUSHIMA METAL WORKS INDONESIA', merge_format)
    worksheet.merge_range('D2:I2', 'DATA KARYAWAN', merge_format)
    # worksheet.write(0, 0, 'Hello, world!')

    # Sheet header, first row
    row_num = 7

    columns = ['no_urut',
               'nopeg',
               'nama_pegawai',
               'npwp',
               'nik',
               'basis_gaji',
               'pph_kembali_bulan_terakhir',
               'jenis_kelamin',
               'status_pajak',
               'status_kepegawaian',
               'dept_code',
               'gl_account',
               'group_expenses',
               'dept',
               'golongan',
               'jabatan',
               'tanggal_mulai_kerja',
               'status_aktif',
               'mulai_payslip',
               'berakhir_payslip',
               'keterangan_aktif',
               'nomor_kontrak',
               'masa_kontrak',
               'status_tidak_aktif',
               'finish_contract',
               'pengganti',
               'keterangan_tidak_aktif',
               'ditempatkan_di_perusahaan',
               'under_supervisi',
               'lokasi',
               'nomor_id_card',
               'masa_berlaku_id_card',
               'tempat_lahir',
               'tanggal_lahir',
               'alamat_domisili_sekarang',
               'alamat_email_perusahaan',
               'alamat_email_pribadi',
               'nomor_telpon',
               'nomor_kartu_keluarga',
               'nama_suami_istri',
               'nama_anak_satu',
               'nama_anak_dua',
               'nama_anak_tiga',
               'nomor_bpjs_kesehatan',
               'masa_berlaku_bpjs_kesehatan',
               'nomor_bpjs_tenagakerja',
               'masa_berlaku_bpjs_tenagakerja',
               'premi_asuransi',
               'pendidikan_terakhir',
               'sertifikat_satu',
               'sertifikat_dua',
               'sertifikat_tiga',
               'sertifikat_empat',
               'training_diikuti',
               'training_satu',
               'training_dua',
               'training_tiga',
               'training_empat',
               'referensi',
               ]

    for col_num in range(len(columns)):
        headers = columns[col_num].replace('_', ' ').upper()
        worksheet.write(row_num, col_num, headers)

    rows = StatusStaff.objects.exclude(staff__access__in=['director', 'hrd', 'finance']).values_list(
        'staff__nopeg',
        'staff__nama_pegawai',
        'staff__npwp',
        'staff__nik',
        'staff__basis_gaji',
        'staff__pph_kembali_bulan_terakhir',
        'staff__jenis_kelamin',
        'staff__status_pajak',
        'staff__status_kepegawaian',
        'staff__dept_code',
        'staff__gl_account',
        'staff__group_expenses',
        'staff__dept',
        'staff__golongan',
        'staff__jabatan',
        'staff__tanggal_mulai_kerja',
        'status_aktif',
        'staff__mulai_payslip',
        'staff__berakhir_payslip',
        'keterangan_aktif',
        'staff__nomor_kontrak',
        'staff__masa_kontrak',
        'status_tidak_aktif',
        'finish_contract',
        'pengganti',
        'keterangan_tidak_aktif',
        'staff__ditempatkan_di_perusahaan',
        'staff__under_supervisi',
        'staff__lokasi',
        'staff__nomor_id_card',
        'staff__masa_berlaku_id_card',
        'staff__tempat_lahir',
        'staff__tanggal_lahir',
        'staff__alamat_domisili_sekarang',
        'staff__alamat_email_perusahaan',
        'staff__alamat_email_pribadi',
        'staff__nomor_telpon',
        'staff__nomor_kartu_keluarga',
        'staff__nama_suami_istri',
        'staff__nama_anak_satu',
        'staff__nama_anak_dua',
        'staff__nama_anak_tiga',
        'staff__nomor_bpjs_kesehatan',
        'staff__masa_berlaku_bpjs_kesehatan',
        'staff__nomor_bpjs_tenagakerja',
        'staff__masa_berlaku_bpjs_tenagakerja',
        'staff__premi_asuransi',
        'staff__pendidikan_terakhir',
        'staff__sertifikat_satu',
        'staff__sertifikat_dua',
        'staff__sertifikat_tiga',
        'staff__sertifikat_empat',
        'staff__training_diikuti',
        'staff__training_satu',
        'staff__training_dua',
        'staff__training_tiga',
        'staff__training_empat',
        'staff__referensi',
    )

    for idx, row in enumerate(rows):
        row_num += 1
        worksheet.write(idx+8, 0, idx+1, content_format)
        for col_num in range(len(row)):
            worksheet.write(row_num, col_num+1,
                            row[col_num], content_format)

    workbook.close()

    output.seek(0)

    response = HttpResponse(output.read(
    ), content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename=Data Karyawan.xls"

    output.close()

    return response


def export_payroll_xlsx(request, month, format):
    response = HttpResponse(content_type='application/ms-excel')
    attachment = 'attachment; filename="Data Gaji %s - %s.xls"' % (
        month, format.upper())
    response['Content-Disposition'] = attachment

    wb = xlwt.Workbook(encoding='utf-8')
    sheet = "Gaji %s" % (month)
    ws = wb.add_sheet(sheet)

    # Sheet header, first row
    row_num = 0
    staff_num = 1

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    ENTER = 2
    RECAP_COL = 27
    SKIP_COL = 5
    START_RECAP = 6
    END_RECAP = 33

    columns = [
        'NO',
        'NPWP',
        'DEP CODE',
        'GL ACCOUNT',
        'GROUP EXPENSES',
        'NAMA',
        'GAJI POKOK',
        'POTONGAN ABSEN',
        'TUNJANGAN PPh 21',
        'LEMBUR',
        'THR',
        'RAPEL',
        'BONUS',
        'UANG MAKAN',
        'T.JABATAN',
        'SENIORITY',
        'TRANSPORT',
        'T.KELUARGA',
        'T.SHIFT',
        'T.KEHADIRAN',
        'BPJS (0.04)',
        'JKK/JKM (1.19%)',
        'TUNJANGAN LAINNYA',
        'GAJI BRUTO',
        'SALARY EXCL. OT',
        'PAJAK PPh 21',
        'ASTEK (0.02 - 0.01)',
        'BPJS (0.01)',
        'BPJS (0.04)',
        'JKK/JKM (1.19%)',
        'IURAN SPSI',
        'POTONGAN LAINNYA',
        'GAJI NETTO',
        'NOMOR REKENING',
        'BANK',
    ]

    for col_num in range(len(columns)):
        if format == 'dept':
            if col_num >= 2 and col_num < END_RECAP:
                ws.write(row_num, col_num, columns[col_num], font_style)
        else:
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()
    font_style.num_format_str = '#,##0.00'

    dept_codes = Staff.objects.all().order_by(
        'dept_code').values('dept_code').distinct()

    total = [0] * RECAP_COL

    for c in dept_codes:
        dept_code = c['dept_code']
        if dept_code != None and len(dept_code) > 0:
            rows = Payroll.objects.filter(staff__dept_code=dept_code, bulan=month, tipe='Gaji Bulanan').values_list(
                'staff__npwp', 'staff__dept_code', 'staff__gl_account', 'staff__group_expenses', 'staff__nama_pegawai',
                'gaji_pokok', 'potongan_absen', 'tunjangan_pph_21', 'lembur', 'thr', 'rapel', 'bonus', 'uang_makan', 'tunjangan_jabatan',
                'seniority', 'transport', 'tunjangan_keluarga', 'tunjangan_shift', 'tunjangan_kehadiran', 'bpjs_kesehatan_4', 'asuransi_jkk_jkm',
                'tunjangan_lainnya', 'gaji_bruto', 'salary_exclude_ot', 'pph_21', 'astek_jp', 'bpjs_kesehatan_1', 'bpjs_kesehatan_4',
                'asuransi_jkk_jkm', 'iuran_spsi', 'potongan_lainnya', 'gaji_netto', 'nomor_rekening', 'bank',
            )

            dept_recap = [0] * RECAP_COL

            for payroll in rows:
                # imutable tuple
                payroll = list(payroll)
                payroll.insert(0, staff_num)

                staff_num += 1

                if format != 'dept':
                    row_num += ENTER

                payroll_thr = Payroll.objects.filter(
                    staff__dept_code=payroll[2], staff__nama_pegawai=payroll[5], bulan=month, tipe='THR').values_list(
                    'staff__npwp', 'staff__dept_code', 'staff__gl_account', 'staff__group_expenses', 'staff__nama_pegawai',
                    'gaji_pokok', 'potongan_absen', 'tunjangan_pph_21', 'lembur', 'thr', 'rapel', 'bonus', 'uang_makan', 'tunjangan_jabatan', 'seniority',
                    'transport', 'tunjangan_keluarga', 'tunjangan_shift', 'tunjangan_kehadiran', 'bpjs_kesehatan_4', 'asuransi_jkk_jkm',
                    'tunjangan_lainnya', 'gaji_bruto', 'salary_exclude_ot', 'pph_21', 'astek_jp', 'bpjs_kesehatan_1', 'bpjs_kesehatan_4',
                    'asuransi_jkk_jkm', 'iuran_spsi', 'potongan_lainnya', 'gaji_netto', 'nomor_rekening', 'bank',
                )

                if payroll_thr:
                    payroll_thr = list(payroll_thr[0])
                    payroll_thr.insert(0, staff_num)

                payroll_bonus = Payroll.objects.filter(
                    staff__dept_code=payroll[2], staff__nama_pegawai=payroll[5], bulan=month, tipe='Bonus').values_list(
                    'staff__npwp', 'staff__dept_code', 'staff__gl_account', 'staff__group_expenses', 'staff__nama_pegawai',
                    'gaji_pokok', 'potongan_absen', 'tunjangan_pph_21', 'lembur', 'thr', 'rapel', 'bonus', 'uang_makan', 'tunjangan_jabatan', 'seniority',
                    'transport', 'tunjangan_keluarga', 'tunjangan_shift', 'tunjangan_kehadiran', 'bpjs_kesehatan_4', 'asuransi_jkk_jkm',
                    'tunjangan_lainnya', 'gaji_bruto', 'salary_exclude_ot', 'pph_21', 'astek_jp', 'bpjs_kesehatan_1', 'bpjs_kesehatan_4',
                    'asuransi_jkk_jkm', 'iuran_spsi', 'potongan_lainnya', 'gaji_netto', 'nomor_rekening', 'bank',
                )

                if payroll_bonus:
                    payroll_bonus = list(payroll_bonus[0])
                    payroll_bonus.insert(0, staff_num)



                for col_num in range(len(payroll)):
                    try:
                        if col_num > SKIP_COL and col_num < END_RECAP:
                            _total = _thr = _bonus = 0

                            _bulanan = float(payroll[col_num])

                            if payroll_thr:
                                _thr = float(payroll_thr[col_num])
                            if payroll_bonus:
                                _bonus = float(payroll_bonus[col_num])

                            _total = _bulanan + _thr + _bonus

                            dept_recap[col_num - START_RECAP] += _total
                            col_write = _total
                        else:
                            col_write = payroll[col_num]
                    except:
                        pass

                    if format != 'dept':
                        # skip 1 row each staff
                        ws.write(row_num, col_num, col_write, font_style)

            row_num += ENTER

            if format == 'dept':
                dept_summary = list(rows[0])[1:4]
                for dept_recap_num in range(len(dept_summary)):
                    ws.write(row_num, dept_recap_num + 2,
                             dept_summary[dept_recap_num], font_style)

            if format == 'dept':
                dept_recap.insert(0, '')
            else:
                dept_recap.insert(0, 'SUBTOTAL')

            for col_sub_num in range(SKIP_COL, len(dept_recap) + SKIP_COL):
                try:
                    if col_sub_num > SKIP_COL:
                        total[col_sub_num -
                              START_RECAP] += dept_recap[col_sub_num - SKIP_COL]
                        col_sub_write = dept_recap[col_sub_num - SKIP_COL]
                    else:
                        col_sub_write = dept_recap[col_sub_num - SKIP_COL]
                except:
                    pass

                ws.write(row_num, col_sub_num, col_sub_write, font_style)

    row_num += ENTER

    total.insert(0, 'TOTAL')

    for total_num in range(SKIP_COL, len(total) + SKIP_COL):
        if total_num > SKIP_COL:
            total_num_write = total[total_num - SKIP_COL]
        else:
            total_num_write = total[total_num - SKIP_COL]

        ws.write(row_num, total_num, total_num_write, font_style)

    wb.save(response)
    return response


def insert_db_pegawai_per_bulan(request):

    codes = Staff.objects.all()\
        .order_by('dept_code')\
        .values('dept_code').distinct()

    for code in codes:
        code = code['dept_code']
        if code != None and len(code) > 0:
            active = StatusStaff.objects.filter(
                staff__dept_code=code, status_pegawai='aktif').count()
            non_active = StatusStaff.objects.filter(
                staff__dept_code=code, status_pegawai='nonaktif').count()
            date = datetime.datetime.now().strftime('%B %Y')
            dashboard = Dashboard(
                jumlah_pegawai_aktif=active,
                jumlah_pegawai_tidak_aktif=non_active,
                unit=code,
                date=date
            )

            dashboard.save()

    return HttpResponse("Automated")

    # start = '01'
    # current_month = datetime.datetime.now()
    # response = {}
    # lstUnit = list()
    # months = get_months_delta(current_month.strftime(
    #     '%Y')+'-'+start, (current_month - dateutil.relativedelta.relativedelta(months=1)).strftime('%Y-%m'))

    # unitsFetch = get_units()
    # for units in unitsFetch:
    #     lstUnit.insert(0, str(units[0])[:2])
    # response['aktif_avg'] = get_all_staff_all_unit(
    #     months, 'jumlah_pegawai_aktif') + count_pegawai('aktif')
    # response['aktif_current'] = count_pegawai('aktif')
    # response['nonaktif_avg'] = get_all_staff_all_unit(
    #     months, 'jumlah_pegawai_tidak_aktif') + count_pegawai('nonaktif')
    # response['jumlah_unit'] = len(lstUnit)
    # response['units'] = lstUnit

    # chart 1
    # data_aktif = []
    # data_nonaktif = []
    # months_chart = get_months_delta(current_month.strftime(
    #     '%Y')+'-'+start, (current_month - dateutil.relativedelta.relativedelta(months=1)).strftime('%Y-%m'))

    # data_aktif.append(count_pegawai('aktif')
    #                   if count_pegawai('aktif') is not None else 0)
    # data_nonaktif.append(count_pegawai('nonaktif')
    #                      if count_pegawai('nonaktif') is not None else 0)
    # for month in months_chart:
    #     data_aktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_aktif') if get_all_staff_all_unit(
    #         [month], 'jumlah_pegawai_aktif') is not None else 0)
    #     data_nonaktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_tidak_aktif') if get_all_staff_all_unit(
    #         [month], 'jumlah_pegawai_tidak_aktif') is not None else 0)
    # months_chart.insert(0, datetime.date.today().strftime("%B %Y"))
    # months_chart.reverse()
    # data_nonaktif.reverse()
    # data_aktif.reverse()
    # response['label'] = months_chart
    # response['data_aktif'] = data_aktif
    # response['data_nonaktif'] = data_nonaktif

    # chart 2
    # default aktif
    # dictio = {}
    # for unit in lstUnit:
    #     dictio[unit] = count_pegawai_per_unit('aktif', unit)
    # sorted_dict = sorted(
    #     dictio.items(), key=operator.itemgetter(1), reverse=True)
    # top10 = sorted_dict[:10]
    # unit = []
    # staff = []
    # for i in top10:
    #     unit.append(i[0])
    #     staff.append(i[1])

    # response['chart_unit'] = unit
    # response['chart_staff'] = staff

    return render(request, 'content/dashboard/hrd.html', response)


@csrf_exempt
def load_dashboard_hrd(request):
    print(1)
    start = '01'

    if request.method == 'POST':
        end = request.POST['date']
        unit = request.POST['unit']
        end_formatted = datetime.date(int(end[:4]), int(end[5:]), 1)

        months = get_months_delta(end_formatted.strftime('%Y')+'-'+start, end)

        if unit == 'all':
            response['aktif_avg'] = get_all_staff_all_unit(
                months, 'jumlah_pegawai_aktif')
            response['aktif_current'] = get_all_staff_all_unit(
                [end_formatted.strftime('%B %Y')], 'jumlah_pegawai_aktif')
            response['nonaktif_avg'] = get_all_staff_all_unit(
                months, 'jumlah_pegawai_tidak_aktif')
            response['jumlah_unit'] = len(
                Dashboard.objects.values('unit').distinct())

        else:
            response['aktif_avg'] = get_all_staff_by_unit(
                unit, months, 'jumlah_pegawai_aktif')
            response['aktif_current'] = get_all_staff_by_unit(
                unit, [end_formatted.strftime('%B %Y')], 'jumlah_pegawai_aktif')
            response['nonaktif_avg'] = get_all_staff_by_unit(
                unit, months, 'jumlah_pegawai_tidak_aktif')
            response['jumlah_unit'] = 1

        html = render_to_string(
            'content/dashboard/dashboard_hrd.html', response)
        return HttpResponse(html)
    return JsonResponse({'succes': 'yes'})


@csrf_exempt
def load_dashboard_hrd_current(request):
    print(1)
    start = '01'

    if request.method == 'POST':
        end = str(datetime.date.today().strftime("%Y-%m"))
        unit = request.POST['unit']
        end_formatted = datetime.date(int(end[:4]), int(
            end[5:]), 1) - dateutil.relativedelta.relativedelta(months=1)
        months = get_months_delta(end_formatted.strftime('%Y')+'-'+start, end)

        if unit == 'all':
            response['aktif_avg'] = get_all_staff_all_unit(
                months, 'jumlah_pegawai_aktif') + count_pegawai('aktif')
            response['aktif_current'] = count_pegawai('aktif')
            response['nonaktif_avg'] = get_all_staff_all_unit(
                months, 'jumlah_pegawai_tidak_aktif') + count_pegawai('nonaktif')
            response['jumlah_unit'] = len(get_units())

        else:
            response['aktif_avg'] = get_all_staff_by_unit(
                unit, months, 'jumlah_pegawai_aktif') + count_pegawai_per_unit('aktif', unit)
            response['aktif_current'] = count_pegawai_per_unit('aktif', unit)
            response['nonaktif_avg'] = get_all_staff_by_unit(
                unit, months, 'jumlah_pegawai_tidak_aktif') + count_pegawai_per_unit('nonaktif', unit)
            response['jumlah_unit'] = 1

        html = render_to_string(
            'content/dashboard/dashboard_hrd.html', response)
        return HttpResponse(html)
    return JsonResponse({'succes': 'yes'})


def load_chart_hrd(request):
    print('load_chart_hrd')
    response = {}
    if request.method == 'POST':
        date_start = request.POST['date_start']
        date_end = request.POST['date_end']
        unit = request.POST['unit']
        months = get_months_delta(date_start, date_end)

        data_aktif = []
        data_nonaktif = []

        if unit == 'all':
            for month in months:
                data_aktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_aktif') is not None else 0)
                data_nonaktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_tidak_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_tidak_aktif') is not None else 0)
        else:
            for month in months:
                data_aktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_aktif') is not None else 0)
                data_nonaktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_tidak_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_tidak_aktif') is not None else 0)

        months.reverse()
        data_nonaktif.reverse()
        data_aktif.reverse()
        response['label'] = months
        response['data_aktif'] = data_aktif
        response['data_nonaktif'] = data_nonaktif

        return HttpResponse(json.dumps(response))
    return JsonResponse({'succes': 'yes'})


def load_chart_hrd_current(request):
    print('load_chart_hrd_current')
    response = {}
    if request.method == 'POST':
        end = str(datetime.date.today().strftime("%Y-%m"))
        date_end = datetime.date(int(end[:4]), int(
            end[5:]), 1) - dateutil.relativedelta.relativedelta(months=1)
        date_start = date_end.strftime('%Y')+'-01'
        unit = request.POST['unit']
        months = get_months_delta(date_start, date_end.strftime("%Y-%m"))

        data_aktif = []
        data_nonaktif = []
        if unit == 'all':
            data_aktif.append(count_pegawai('aktif')
                              if count_pegawai('aktif') is not None else 0)
            data_nonaktif.append(count_pegawai(
                'nonaktif') if count_pegawai('nonaktif') is not None else 0)
            for month in months:
                data_aktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_aktif') is not None else 0)
                data_nonaktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_tidak_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_tidak_aktif') is not None else 0)
            months.insert(0, datetime.date.today().strftime("%B %Y"))
        else:
            data_aktif.append(count_pegawai_per_unit(
                'aktif', unit) if count_pegawai_per_unit('aktif', unit) is not None else 0)
            data_nonaktif.append(count_pegawai_per_unit(
                'nonaktif', unit) if count_pegawai_per_unit('nonaktif', unit) is not None else 0)
            for month in months:
                data_aktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_aktif') is not None else 0)
                data_nonaktif.append(get_all_staff_all_unit([month], 'jumlah_pegawai_tidak_aktif') if get_all_staff_all_unit(
                    [month], 'jumlah_pegawai_tidak_aktif') is not None else 0)
            months.insert(0, datetime.date.today().strftime("%B %Y"))

        months.reverse()
        data_nonaktif.reverse()
        data_aktif.reverse()
        response['label'] = months
        response['data_aktif'] = data_aktif
        response['data_nonaktif'] = data_nonaktif

        return HttpResponse(json.dumps(response))
    return JsonResponse({'succes': 'yes'})


def load_chart_unit(request):
    print('load_chart_unit')
    response = {}
    if request.method == 'POST':
        date = request.POST['date']
        status = request.POST['status']
        date_formatted = datetime.date(
            int(date[:4]), int(date[5:]), 1).strftime('%B %Y')
        lstUnit = []

        unitsFetch = Dashboard.objects.values('unit').distinct()

        for units in unitsFetch:
            lstUnit.insert(0, str(units['unit']))
        dictio = {}

        if status == 'aktif':
            for unit in lstUnit:
                dictio[unit] = get_all_staff_by_unit(
                    unit, [date_formatted], 'jumlah_pegawai_aktif')
        else:
            for unit in lstUnit:
                dictio[unit] = get_all_staff_by_unit(
                    unit, [date_formatted], 'jumlah_pegawai_tidak_aktif')

        sorted_dict = sorted(
            dictio.items(), key=operator.itemgetter(1), reverse=True)
        top10 = sorted_dict[:10]
        unit = []
        staff = []
        for i in top10:
            unit.append(i[0])
            staff.append(i[1])

        response['chart_unit'] = unit
        response['chart_staff'] = staff

        return HttpResponse(json.dumps(response))
    return JsonResponse({'succes': 'yes'})


@login_required
@permission_required('ui.view_staff', raise_exception=True)
def dashboard_staff(request):

    items = [
        'jumlah_pegawai_aktif',
        'jumlah_pegawai_tidak_aktif',
    ]

    dst = Dashboard.objects.all().values('date').distinct()
    codes = Dashboard.objects.all().values('unit').distinct()
    dept_codes = [code['unit']
                  for code in codes
                  if code['unit'] != None
                  if len(code['unit']) > 0
                  ]

    recap_code = request.GET.get('recap_code')
    recap_month = request.GET.get('recap_month')

    if recap_month == None:
        _, month = get_min_max_month(dst)
    else:
        month = recap_month

    if recap_code == None:
        code_list = dept_codes
    else:
        code_list = [recap_code]

    try:
        year = month.year
    except:
        year = datetime.datetime.strptime(month, "%B %Y").year

    start = datetime.datetime(year, 1, 1).strftime("%B %Y")
    list_bulan = get_month_list(start, month)

    for item in items:
        year = Dashboard.objects\
            .filter(unit__in=code_list, date__in=list_bulan)\
            .aggregate(count=Sum(item))

        response[f'{item}_tahun'] = year['count'] if year['count'] != None else 0

        month = Dashboard.objects\
            .filter(unit__in=code_list, date=list_bulan[0])\
            .aggregate(count=Sum(item))

        response[f'{item}_bulan'] = month['count'] if month['count'] != None else 0

    response['total_unit'] = len(dept_codes)

    ##################### CHART 1 ###########################

    stats_code = request.GET.get('stats_code')
    stats_start = request.GET.get('stats_start')
    stats_end = request.GET.get('stats_end')

    if stats_start == None and stats_end == None:
        start, end = get_min_max_month(dst)
    elif stats_start == None and stats_end != None:
        start, _ = get_min_max_month(dst)
        end = stats_end
    elif stats_start != None and stats_end == None:
        _, end = get_min_max_month(dst)
        start = stats_start
    else:
        start = stats_start
        end = stats_end

    if stats_code == None:
        code_list = dept_codes
    else:
        code_list = [stats_code]

    list_bulan = get_month_list(start, end)
    list_bulan.reverse()

    stats = list()
    for bulan in list_bulan:
        monthly_stats = [bulan]
        for item in items:
            month = Dashboard.objects\
                .filter(unit__in=code_list, date=bulan)\
                .aggregate(count=Sum(item))

            month = month['count'] if month['count'] != None else 0
            monthly_stats.append(month)

        stats.append(monthly_stats)

    response['stats'] = stats

    ##################### CHART 2 ###########################

    status_code = request.GET.get('status_code')
    status_month = request.GET.get('status_month')

    if status_month == None:
        _, month = get_min_max_month(dst)
    else:
        month = status_month

    if status_code == None:
        status_code = 'jumlah_pegawai_aktif'
    else:
        status_code = 'jumlah_pegawai_aktif' if status_code == 'aktif' else 'jumlah_pegawai_tidak_aktif'

    code_list = dept_codes

    try:
        month = month.strftime("%B %Y")
    except:
        pass

    statuses = list()

    for code in code_list:
        status = Dashboard.objects\
            .filter(unit=code, date=month)\
            .aggregate(count=Sum(status_code))

        status = status['count'] if status['count'] != None else 0
        statuses.append((code, status))

    response['status'] = statuses

    s_min, e_max = get_min_max_month(dst)
    months = get_month_list(s_min, e_max)

    response['months'] = months
    response['dept_code'] = dept_codes

    response['months_val'] = convert_to_datetime(months)

    response['recap_code'] = recap_code
    response['recap_month'] = recap_month

    response['recap_month_val'] = convert_to_datetime(recap_month)

    response['stats_code'] = stats_code
    response['stats_start'] = stats_start
    response['stats_end'] = stats_end

    response['stats_start_val'] = convert_to_datetime(stats_start)
    response['stats_end_val'] = convert_to_datetime(stats_end)

    response['status_code'] = 'aktif' if status_code == 'jumlah_pegawai_aktif' else 'nonaktif'
    response['status_month'] = status_month

    response['status_month_val'] = convert_to_datetime(status_month)

    return render(request, 'content/dashboard/hrd.html', response)


@login_required
@permission_required('ui.view_payroll', raise_exception=True)
def dashboard_payroll(request):
    response = {}

    recap_code = request.GET.get('recap_code')
    recap_start = request.GET.get('recap_start')
    recap_end = request.GET.get('recap_end')

    items = [
        'gaji_pokok',
        'potongan_absen',
        'lembur',
        'thr',
        'rapel',
        'bonus',
        'uang_makan',
        'tunjangan_jabatan',
        'seniority',
        'transport',
        'tunjangan_keluarga',
        'tunjangan_shift',
        'tunjangan_kehadiran',
        'tunjangan_lainnya',
        'tunjangan_pph_21',
        'gaji_bruto',
        'salary_exclude_ot',
        'pph_21',
        'astek_jp',
        'bpjs_kesehatan_1',
        'bpjs_kesehatan_4',
        'asuransi_jkk_jkm',
        'iuran_spsi',
        'potongan_lainnya',
        'total_potongan',
        'gaji_netto',
    ]

    dst = Payroll.objects.all().values('bulan').distinct()

    codes = Staff.objects.all().order_by('dept_code').values('dept_code').distinct()
    dept_codes = [code['dept_code']
                  for code in codes
                  if code['dept_code'] != None
                  if len(code['dept_code']) > 0
                  ]

    if recap_start == None and recap_end == None:
        start, end = get_min_max_month(dst)
    elif recap_start == None and recap_end != None:
        start, _ = get_min_max_month(dst)
        end = recap_end
    elif recap_start != None and recap_end == None:
        _, end = get_min_max_month(dst)
        start = recap_start
    else:
        start = recap_start
        end = recap_end

    if recap_code == None:
        code_list = dept_codes
    else:
        code_list = [recap_code]

    list_bulan = get_month_list(start, end)

    for item in items:
        bulanan = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan__in=list_bulan)\
            .filter(tipe='Gaji Bulanan')\
            .aggregate(
                total=Sum(Cast(item, output_field=FloatField()))
            )

        thr = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan__in=list_bulan)\
            .filter(tipe='THR')\
            .aggregate(
                total=Sum(Cast(item, output_field=FloatField()))
            )

        bonus = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan__in=list_bulan)\
            .filter(tipe='Bonus')\
            .aggregate(
                total=Sum(Cast(item, output_field=FloatField()))
            )

        response[item] = payroll_sum(
            bulanan['total'], thr['total'], bonus['total'])

    ###################### CHART ######################

    rate_code = request.GET.get('rate_code')
    rate_start = request.GET.get('rate_start')
    rate_end = request.GET.get('rate_end')

    if rate_start == None and rate_end == None:
        start, end = get_min_max_month(dst)
    elif rate_start == None and rate_end != None:
        start, _ = get_min_max_month(dst)
        end = rate_end
    elif rate_start != None and rate_end == None:
        _, end = get_min_max_month(dst)
        start = rate_start
    else:
        start = rate_start
        end = rate_end

    if rate_code == None:
        code_list = dept_codes
    else:
        code_list = [rate_code]

    list_bulan = reversed(get_month_list(start, end))

    rates = list()

    for bulan in list_bulan:
        bulanan = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan=bulan)\
            .filter(tipe='Gaji Bulanan')\
            .aggregate(
                total=Sum(Cast('gaji_netto', output_field=FloatField()))
            )

        thr = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan=bulan)\
            .filter(tipe='THR')\
            .aggregate(
                total=Sum(Cast('gaji_netto', output_field=FloatField()))
            )

        bonus = Payroll.objects\
            .filter(staff__dept_code__in=code_list)\
            .filter(bulan=bulan)\
            .filter(tipe='Bonus')\
            .aggregate(
                total=Sum(Cast('gaji_netto', output_field=FloatField()))
            )

        rate = payroll_sum(
            bulanan['total'], thr['total'], bonus['total'])

        rates.append((rate, bulan))

    response['rates'] = rates

    s_min, e_max = get_min_max_month(dst)
    months = get_month_list(s_min, e_max)

    response['months'] = months
    response['dept_code'] = dept_codes

    response['months_val'] = convert_to_datetime(months)

    response['recap_code'] = recap_code
    response['recap_start'] = recap_start
    response['recap_end'] = recap_end

    print(convert_to_datetime(recap_start))

    response['recap_start_val'] = convert_to_datetime(recap_start)
    response['recap_end_val'] = convert_to_datetime(recap_end)

    response['rate_start'] = rate_start
    response['rate_end'] = rate_end

    response['rate_start_val'] = convert_to_datetime(rate_start)
    response['rate_end_val'] = convert_to_datetime(rate_end)

    # print(p)
    # lstUnit = list()
    # month = datetime.datetime.now().strftime('%B %Y')
    # unitsFetch = get_units()
    # for units in unitsFetch:
    #     lstUnit.insert(0, str(units[0])[:2])

    # response = get_payroll_dashboard(month)
    # response['units'] = lstUnit

    # chart

    # initial_month = datetime.date(2019, 11, 25).strftime('%Y-%m')
    # current_month = datetime.datetime.now().strftime('%Y-%m')

    # months = get_months_delta(initial_month, current_month)
    # response['label'] = months
    # data = list()
    # for month in months:
    #     data.append(get_payroll_all_unit([month], 'gaji_bersih') if get_payroll_all_unit(
    #         [month], 'gaji_bersih') is not None else 0)

    # months.reverse()
    # data.reverse()

    # response['data'] = data

    return render(request, 'content/dashboard/payroll.html', response)


@csrf_exempt
def load_dashboard_payroll(request):
    print(1)
    if request.method == 'POST':
        date_start = request.POST['date_start']
        date_end = request.POST['date_end']
        unit = request.POST['unit']

        months = get_months_delta(date_start, date_end)

        if unit == 'all':
            response = get_payroll_dashboard(months)
        else:
            response = get_payroll_dashboard_unit(months, unit)

        html = render_to_string(
            'content/dashboard/dashboard_payroll.html', response)
        return HttpResponse(html)
    return JsonResponse({'succes': 'yes'})


def load_chart_payroll(request):
    print('load_chart_payroll')
    response = {}
    if request.method == 'POST':
        date_start = request.POST['date_start']
        date_end = request.POST['date_end']
        unit = request.POST['unit']
        months = get_months_delta(date_start, date_end)
        response['label'] = months
        data = list()

        if unit == 'all':
            for month in months:
                data.append(get_payroll_all_unit(month, 'gaji_bersih') if get_payroll_all_unit(
                    month, 'gaji_bersih') is not None else 0)
        else:
            for month in months:
                data.append(get_payroll_by_unit(unit, month, 'gaji_bersih') if get_payroll_by_unit(
                    unit, month, 'gaji_bersih') is not None else 0)
        months.reverse()
        data.reverse()
        response['data'] = data
        return HttpResponse(json.dumps(response))
    return JsonResponse({'succes': 'yes'})


def webmail_redirect(request):
    return redirect('https://sgx15.dewaweb.com:2096/')


# def export_to_dashboard(self):

class Pdf(View):
    def get(self, request, nopeg, bulan, tipe):
        payroll = Payroll.objects.filter(
            staff__nopeg=nopeg, bulan=bulan, tipe=tipe, status="Published")
        date = datetime.date.today()
        params = {
            'payroll': payroll,
            'date': date,
        }
        return Render.render('content/payroll/pdf.html', params)


class BulkPdf(View):
    def get(self, request, bulan, tipe):
        payroll = Payroll.objects.filter(
            bulan=bulan, tipe=tipe, status="Published")
        date = datetime.date.today()
        params = {
            'payroll': payroll,
            'date': date,
        }
        return Render.render('content/payroll/pdf.html', params)


class StaffBulkPdf(View):
    def get(self, request):
        staffs = Staff.objects.all()
        date = datetime.date.today()
        params = {
            'staffs': staffs,
            'date': date,
        }
        return Render.render('content/staff/pdf.html', params)
