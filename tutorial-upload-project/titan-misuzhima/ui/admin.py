from django.contrib import admin
from ui.models import Staff, Payroll, History, StatusStaff, Dashboard
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q


class SingletonModelAdmin(object):
    def has_add_permission(self, request):
        num_objects = self.model.objects.count()
        if num_objects >= 1:
            return False
        return super(SingletonModelAdmin, self).has_add_permission(request)


class InputFilter(admin.SimpleListFilter):
    template = 'admin/input_filter.html'

    def lookups(self, request, model_admin):
        # Dummy, required to show the filter.
        return ((),)

    def choices(self, changelist):
        # Grab only the "all" option.
        all_choice = next(super().choices(changelist))
        all_choice['query_parts'] = (
            (k, v)
            for k, v in changelist.get_filters_params().items()
            if k != self.parameter_name
        )
        yield all_choice


class IDFilter(InputFilter):
    parameter_name = ('id')
    title = _('id')

    def queryset(self, request, queryset):
        if self.value() is not None:
            uid = self.value()
            return queryset.filter(
                Q(id=uid)
            )


class NopegFilter(InputFilter):
    parameter_name = ('nopeg')
    title = _('nopeg')

    def queryset(self, request, queryset):
        if self.value() is not None:
            uid = self.value()
            return queryset.filter(
                Q(nopeg__contains=uid)
            )


class NopegPayrollFilter(InputFilter):
    parameter_name = ('nopeg')
    title = _('nopeg')

    def queryset(self, request, queryset):
        if self.value() is not None:
            uid = self.value()
            return queryset.filter(
                Q(staff__nopeg__contains=uid)
            )


class BulanFilter(InputFilter):
    parameter_name = ('bulan')
    title = _('bulan')

    def queryset(self, request, queryset):
        if self.value() is not None:
            uid = self.value()
            return queryset.filter(
                Q(bulan__icontains=uid)
            )

# # Register your models here.
@admin.register(Payroll)
class Payroll(SingletonModelAdmin, admin.ModelAdmin):
    list_filter = (NopegPayrollFilter, BulanFilter)
    list_display = ('get_nopeg', 'bulan')
    # list_display_links = ('id', 'deskripsi')
    list_per_page = 25

    def get_nopeg(self, obj):
        return obj.staff.nopeg
    get_nopeg.noped = 'nopeg'
    get_nopeg.admin_order_field = 'staff__nopeg'

# # Register your models here.
@admin.register(Staff)
class DeskripsiSingkatAdmin(SingletonModelAdmin, admin.ModelAdmin):
    list_filter = (IDFilter, NopegFilter, 'status_kepegawaian')
    list_display = ('id', 'nopeg', 'status_kepegawaian')
    # list_display_links = ('id', 'deskripsi')
    list_per_page = 25


# admin.site.register(History)
# admin.site.register(Dashboard)
