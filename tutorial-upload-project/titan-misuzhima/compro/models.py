from django.db import models
from django.core.cache import cache


class SingletonModel(object):
    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        pass

    def set_cache(self):
        cache.set(self.__class__.__name__, self)

    def save(self, *args, **kwargs):
        self.pk = 1
        super(SingletonModel, self).save(*args, **kwargs)

        self.set_cache()

    @classmethod
    def load(cls):
        if cache.get(cls.__name__) is None:
            obj, created = cls.objects.get_or_create(pk=1)
            if not created:
                obj.set_cache()
        return cache.get(cls.__name__)


class HeroImage(models.Model):
    BERANDA = 'BERANDA'
    PROFIL = 'PROFIL'
    KEBIJAKAN = 'KEBIJAKAN'
    KARIR = 'KARIR'
    PILIHAN_HALAMAN = (
        (BERANDA, 'Beranda'),
        (PROFIL, 'Profil Petinggi'),
        (KEBIJAKAN, 'Kebijakan Mutu'),
        (KARIR, 'Karir'),
    )
    jenis_halaman = models.CharField(max_length=20, choices=PILIHAN_HALAMAN, default=BERANDA, unique=True,
                                     error_messages={'unique': 'Satu halaman hanya memiliki 1 hero image saja'})
    gambar = models.ImageField()

    class Meta:
        verbose_name_plural = 'Hero Images'


class DeskripsiSingkat(SingletonModel, models.Model):
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Deskripsi Singkat'


class NilaiPerusahaan(models.Model):
    gambar = models.ImageField()
    judul = models.CharField(max_length=50)
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Nilai Perusahaan'


class Testimoni(models.Model):
    foto = models.ImageField()
    nama = models.CharField(max_length=50)
    jabatan = models.CharField(max_length=100)
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Testimoni'


class FotoGaleri(models.Model):
    foto = models.ImageField()
    deskripsi = models.TextField(max_length=500)

    class Meta:
        verbose_name_plural = 'Foto Galeri'


class DeskripsiLengkap(SingletonModel, models.Model):
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Deskripsi Lengkap'


class VisiMisi(models.Model):
    VISI = 'VISI'
    MISI = 'MISI'
    PILIHAN_VISI_MISI = (
        (VISI, 'Visi'),
        (MISI, 'Misi'),
    )
    jenis = models.CharField(max_length=20, choices=PILIHAN_VISI_MISI, default=VISI, unique=True,
                             error_messages={'unique': 'Visi atau Misi hanya bisa satu saja'})
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Visi Misi'


class StrukturOrganisasi(SingletonModel, models.Model):
    gambar = models.ImageField()

    class Meta:
        verbose_name_plural = 'Struktur Organisasi'

class ProfilPetinggi(models.Model):
    foto = models.ImageField()
    nama = models.CharField(max_length=50)
    jabatan = models.CharField(max_length=100)
    deskripsi = models.TextField(null=True)

    class Meta:
        verbose_name_plural = 'Profil Petinggi'


class KebijakanMutu(SingletonModel, models.Model):
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Kebijakan Mutu'


class InfoKarir(SingletonModel, models.Model):
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Info Karir'


class Alamat(SingletonModel, models.Model):
    deskripsi = models.TextField()

    class Meta:
        verbose_name_plural = 'Alamat'


class Kontak(models.Model):
    TELEPON = 'TELP'
    FAX = 'FAX'
    PILIHAN_JENIS_KONTAK = (
        (TELEPON, 'Telepon'),
        (FAX, 'Fax'),
    )
    jenis = models.CharField(max_length=20, choices=PILIHAN_JENIS_KONTAK, default=TELEPON)
    nomor = models.CharField(max_length=20)

    class Meta:
        verbose_name_plural = 'Kontak'


class SosialMedia(models.Model):
    FACEBOOK = 'FB'
    TWITTER = 'TWITTER'
    GOOGLE_PLUS = 'GPLUS'
    PILIHAN_SOSIAL_MEDIA = (
        (FACEBOOK, 'Facebook'),
        (TWITTER, 'Twitter'),
        (GOOGLE_PLUS, 'Google Plus'),
    )
    jenis = models.CharField(max_length=20, choices=PILIHAN_SOSIAL_MEDIA, default=FACEBOOK, unique=True,
                             error_messages={'unique': 'Jenis Sosial Media ini sudah terdaftar'})
    url = models.URLField()

    class Meta:
        verbose_name_plural = 'Sosial Media'
