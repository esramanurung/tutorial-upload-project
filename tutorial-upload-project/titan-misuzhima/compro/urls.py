from django.urls import path
from .views import beranda, profil_petinggi, kebijakan_mutu, karir

app_name='compro'
urlpatterns = [
    path('', beranda, name="beranda"),
    path('profil/', profil_petinggi, name="profil"),
    path('kebijakan-mutu/', kebijakan_mutu, name="kebijakan-mutu"),
    path('karir/', karir, name="karir")
]