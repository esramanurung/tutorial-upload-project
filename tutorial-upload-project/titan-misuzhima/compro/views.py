from django.shortcuts import render
from .models import HeroImage, DeskripsiSingkat, DeskripsiLengkap, NilaiPerusahaan, Testimoni, FotoGaleri, VisiMisi, \
    ProfilPetinggi, Alamat, Kontak, SosialMedia, KebijakanMutu, StrukturOrganisasi, InfoKarir


def beranda(request):
    response = init_response(page_title='Beranda')

    if DeskripsiSingkat.objects.all().count() > 0:
        deskripsi_singkat = DeskripsiSingkat.objects.all().last()
        response['deskripsi_singkat'] = deskripsi_singkat

    nilai_perusahaan = NilaiPerusahaan.objects.all()[:4]
    foto_galeri = FotoGaleri.objects.all()
    testimoni = Testimoni.objects.all()
    response['nilai_perusahaan'] = nilai_perusahaan
    response['foto_galeri'] = foto_galeri
    response['testimoni'] = testimoni[testimoni.count() - 3:] if testimoni.count() > 2 else testimoni
    return render(request, 'beranda.html', response)


def profil_petinggi(request):
    response = init_response(page_title='Profil Petinggi')

    if DeskripsiLengkap.objects.all().count() > 0:
        deskripsi_lengkap = DeskripsiLengkap.objects.all().last()
        response['deskripsi_lengkap'] = deskripsi_lengkap
    if VisiMisi.objects.filter(jenis='VISI').exists():
        visi = VisiMisi.objects.get(jenis='VISI')
        response['visi'] = visi
    if VisiMisi.objects.filter(jenis='MISI').exists():
        misi = VisiMisi.objects.get(jenis='MISI')
        response['misi'] = misi
    if StrukturOrganisasi.objects.all().count() > 0:
        struktur_organisasi = StrukturOrganisasi.objects.all().last()
        response['struktur_organisasi'] = struktur_organisasi

    profil_petinggi = ProfilPetinggi.objects.all()[:5]
    response['profil_petinggi'] = profil_petinggi
    return render(request, 'profil-petinggi.html', response)


def kebijakan_mutu(request):
    response = init_response(page_title='Kebijakan Mutu')

    if KebijakanMutu.objects.all().count() > 0:
        kebijakan_mutu = KebijakanMutu.objects.all().last()
        response['kebijakan_mutu'] = kebijakan_mutu

    return render(request, 'kebijakan-mutu.html', response)


def karir(request):
    response = init_response(page_title='Karir')

    if InfoKarir.objects.all().count() > 0:
        info_karir = InfoKarir.objects.all().last()
        response['info_karir'] = info_karir

    return render(request, 'karir.html', response)


def init_response(page_title):
    response = {'title': page_title}

    if HeroImage.objects.filter(jenis_halaman=page_title.split(" ")[0].upper()):
        hero_img = HeroImage.objects.get(jenis_halaman=page_title.split(" ")[0].upper())
        response['hero_img'] = hero_img
    if Alamat.objects.all().count() > 0:
        alamat = Alamat.objects.all().last()
        response['alamat'] = alamat
    if Kontak.objects.filter(jenis='TELP').exists():
        telepon = Kontak.objects.filter(jenis='TELP')
        response['telepon'] = telepon
    if Kontak.objects.filter(jenis='FAX').exists():
        fax = Kontak.objects.filter(jenis='FAX')
        response['fax'] = fax
    if SosialMedia.objects.filter(jenis='FB').exists():
        fb = SosialMedia.objects.get(jenis='FB')
        response['fb'] = fb
    if SosialMedia.objects.filter(jenis='TWITTER').exists():
        twitter = SosialMedia.objects.get(jenis='TWITTER')
        response['twitter'] = twitter
    if SosialMedia.objects.filter(jenis='GPLUS').exists():
        gplus = SosialMedia.objects.get(jenis='GPLUS')
        response['gplus'] = gplus

    return response

